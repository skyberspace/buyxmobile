import 'package:flutter/material.dart';
import 'package:getir_app/loggedInState/modals/userModal.dart';

class LoggedInState with ChangeNotifier {
  UserModal _user;
  bool _isLoggedIn = false;

  UserModal get user => _user;
  bool get isLoggedIn => _isLoggedIn;

  addFirst(UserModal user) {
    _user = user;
    _isLoggedIn = true;
    notifyListeners();
  }

  deleteUser() {
    _user = null;
    _isLoggedIn = false;
    notifyListeners();
  }

  checkLoggedIn() {
    print(_isLoggedIn);
    return _isLoggedIn;
  }
}
