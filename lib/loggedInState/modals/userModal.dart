import 'package:flutter/material.dart';
import 'package:getir_app/profilePage/view/widgets/adress/modals/addressModal.dart';

class UserModal {
  String phoneNumber;
  String name;
  String mail;
  List<adressModal> adresses;
  UserModal({@required this.phoneNumber, this.name, this.mail, this.adresses});
}
