import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class PopularSearchItem extends StatelessWidget {
  const PopularSearchItem({
    @required this.givenText,
  });
  final String givenText;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              givenText,
              style: TextStyle(
                color: kPurpleColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
