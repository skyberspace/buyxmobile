import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:getir_app/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:provider/provider.dart';
import 'modals/popularSearchItemModal.dart';
import 'widgets/popularSearchItems.dart';

class SearchBuyxPage extends StatefulWidget {
  @override
  _SearchBuyxPageState createState() => _SearchBuyxPageState();
}

class _SearchBuyxPageState extends State<SearchBuyxPage> {
  List<PopularSearchItemModal> items = [
    PopularSearchItemModal(name: "yumurta"),
    PopularSearchItemModal(name: "yoğurt"),
    PopularSearchItemModal(name: "kahve"),
    PopularSearchItemModal(name: "peynir"),
    PopularSearchItemModal(name: "cips"),
    PopularSearchItemModal(name: "su"),
    PopularSearchItemModal(name: "dondurma"),
    PopularSearchItemModal(name: "ekmek"),
    PopularSearchItemModal(name: "sucuk"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              Text(
                "Arama",
                style: TextStyle(color: Colors.white),
              ),
              Consumer<CartBuyxState>(
                  builder: (context, state, widget) =>
                      state.items.isNotEmpty == true
                          ? GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CartPageBuyx()));
                              },
                              child: Container(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(4),
                                            topLeft: Radius.circular(4)),
                                        color: Colors.white,
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.shopping_basket,
                                          color: kPurpleColor,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(4),
                                            topRight: Radius.circular(4)),
                                        color: kBrightPurpleColor,
                                      ),
                                      child: Center(
                                        child: Text(
                                          state.sumOfAll.toString(),
                                          style: TextStyle(color: kPurpleColor),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox()),
            ],
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Material(
            elevation: 10,
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 13.5,
              color: Colors.white,
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Icon(Icons.search, color: kPurpleColor),
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Ürün Ara",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  Expanded(flex: 1, child: Icon(Icons.mic))
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, top: 30),
            child: Text(
              "Popüler Aramalar",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.grey,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 10,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Center(
              child: ListView.builder(
                primary: false,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return PopularSearchItem(
                    givenText: items[index].name,
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
