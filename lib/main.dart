import 'package:flutter/material.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:getir_app/splash/splash.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      child: MaterialApp(
        theme: ThemeData(
            primaryColor: Colors.white,
            accentColor: Colors.white,
            scaffoldBackgroundColor: Colors.white),
        home: splashScreen(),
      ),
      providers: [
        ChangeNotifierProvider(
          create: (context) => CartBuyxState(),
        ),
        ChangeNotifierProvider(
          create: (context) => CartBuyxFoodState(),
        ),
        ChangeNotifierProvider(
          create: (context) => LoggedInState(),
        ),
      ],
    );
  }
}
