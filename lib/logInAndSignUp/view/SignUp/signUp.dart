import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyx.dart';
import 'package:getir_app/constants.dart';
import 'package:flutter/gestures.dart';
import 'package:getir_app/loggedInState/modals/userModal.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:provider/provider.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool checkedValue = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controllerPhone = TextEditingController();
  final TextEditingController controllerName = TextEditingController();
  final TextEditingController controllerMail = TextEditingController();
  String initialCountry = 'NG';
  PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Üyelik",
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Color(0xff4267b2)),
                            ),
                            onPressed: () {},
                            child: Text(
                              "Facebook ile Bağlan",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: Theme(
                          data: ThemeData(primaryColor: kGreyColor),
                          child: Container(
                            margin: EdgeInsets.all(12),
                            child: InternationalPhoneNumberInput(
                              onInputChanged: (PhoneNumber number) {
                                print(number.phoneNumber);
                              },
                              onInputValidated: (bool value) {
                                print(value);
                              },
                              selectorConfig: SelectorConfig(
                                selectorType:
                                    PhoneInputSelectorType.BOTTOM_SHEET,
                              ),
                              ignoreBlank: false,
                              autoValidateMode:
                                  AutovalidateMode.onUserInteraction,
                              selectorTextStyle: TextStyle(color: Colors.black),
                              initialValue: number,
                              textFieldController: controllerPhone,
                              formatInput: false,
                              keyboardType: TextInputType.numberWithOptions(
                                  signed: true, decimal: true),
                              inputBorder: OutlineInputBorder(),
                              onSaved: (PhoneNumber number) {
                                print('On Saved: $number');
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText:
                                "Girdiğiniz şifre en az 4 karakterden oluşmalı.",
                            hintStyle:
                                TextStyle(fontSize: 14, color: kGreyColor),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: kGreyColor),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerName,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText: "Ad Soyad",
                            hintStyle:
                                TextStyle(fontSize: 14, color: kGreyColor),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: kGreyColor),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerMail,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            } else if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              return 'Please enter a valid E-mail';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText: "E-Posta",
                            hintStyle:
                                TextStyle(fontSize: 14, color: kGreyColor),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: kGreyColor),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: kPurpleColor,
                        title: Text(
                          "Buyx'in bana özel kampanya, tanıtım ve fırsatlarından haberdar olmak istiyorum.",
                          style: TextStyle(fontSize: 12),
                        ),
                        value: checkedValue,
                        onChanged: (newValue) {
                          setState(() {
                            checkedValue = newValue;
                          });
                          print(checkedValue);
                        },
                        controlAffinity: ListTileControlAffinity
                            .leading, //  <-- leading Checkbox
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                          padding: EdgeInsets.all(12),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: "Üye olmakla ",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                TextSpan(
                                  text: "Kullanım Koşullarını",
                                  style: TextStyle(
                                      color: kPurpleColor, fontSize: 12),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () => print('Tap Here onTap'),
                                ),
                                TextSpan(
                                    text: " onaylamış olursunuz.",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                              ],
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                          padding: EdgeInsets.all(12),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text:
                                        "Kişisel verilerinize dair Aydınlatma Metni için ",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                TextSpan(
                                  text: "tıklayınız",
                                  style: TextStyle(
                                      color: kPurpleColor, fontSize: 12),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () => print('Tap Here onTap'),
                                ),
                              ],
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  kPurpleColor),
                            ),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                loggedState.addFirst(UserModal(
                                    phoneNumber: controllerPhone.toString(),
                                    name: controllerName.toString(),
                                    mail: controllerMail.toString()));
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => NavigationBuyx()),
                                  (Route<dynamic> route) => false,
                                );
                              }
                            },
                            child: Text(
                              "Üye Ol",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
