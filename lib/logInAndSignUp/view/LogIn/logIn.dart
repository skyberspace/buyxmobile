import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyx.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/logInAndSignUp/view/SignUp/signUp.dart';
import 'package:getir_app/logInAndSignUp/view/forgotPassword/forgotPassword.dart';
import 'package:getir_app/loggedInState/modals/userModal.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:provider/provider.dart';

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  bool checkedValue = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  String initialCountry = 'NG';
  PhoneNumber number = PhoneNumber(isoCode: 'NG', phoneNumber: "5315817231");
  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Giriş",
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Color(0xff4267b2)),
                        ),
                        onPressed: () {},
                        child: Text(
                          "Facebook ile Bağlan",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Theme(
                    data: ThemeData(primaryColor: kGreyColor),
                    child: Container(
                      margin: EdgeInsets.all(12),
                      child: InternationalPhoneNumberInput(
                        onInputChanged: (PhoneNumber number) {
                          print(number.phoneNumber);
                        },
                        onInputValidated: (bool value) {
                          print(value);
                        },
                        selectorConfig: SelectorConfig(
                          selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                        ),
                        ignoreBlank: false,
                        autoValidateMode: AutovalidateMode.disabled,
                        selectorTextStyle: TextStyle(color: Colors.black),
                        initialValue: number,
                        textFieldController: controllerPhone,
                        formatInput: false,
                        keyboardType: TextInputType.numberWithOptions(
                            signed: true, decimal: true),
                        inputBorder: OutlineInputBorder(),
                        onSaved: (PhoneNumber number) {
                          print('On Saved: $number');
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    margin: EdgeInsets.all(12),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Cant be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        helperText: '',
                        hintText:
                            "Girdiğiniz şifre en az 4 karakterden oluşmalı.",
                        hintStyle: TextStyle(fontSize: 14, color: kGreyColor),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: kGreyColor),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(kPurpleColor),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            loggedState.addFirst(UserModal(
                                phoneNumber: controllerPhone.toString()));
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NavigationBuyx()),
                              (Route<dynamic> route) => false,
                            );
                          }
                        },
                        child: Text(
                          "Giriş Yap",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ForgotPassword(),
                              ));
                        },
                        child: Text("Şifremi Unuttum",
                            style: TextStyle(color: kGreyColor)))),
                Expanded(
                  flex: 7,
                  child: SizedBox(),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.all(12),
                    width: MediaQuery.of(context).size.width,
                    child: OutlinedButton(
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(kPurpleColor),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpPage()));
                      },
                      child: Text("Üye Ol"),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
