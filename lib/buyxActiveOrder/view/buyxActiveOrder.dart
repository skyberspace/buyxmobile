import 'package:flutter/material.dart';

import '../../constants.dart';

class BuyxActiveOrder extends StatefulWidget {
  const BuyxActiveOrder({Key key}) : super(key: key);

  @override
  _BuyxActiveOrderState createState() => _BuyxActiveOrderState();
}

class _BuyxActiveOrderState extends State<BuyxActiveOrder> {
  bool activeOrderState = false;
  getActiveOrder() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Active Orders",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: activeOrderState
            ? ListView.builder(
                itemCount: showList.length,
                itemBuilder: (BuildContext context, int index) {
                  return new Card(
                    child: new Container(
                      padding: new EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          new ListTile(
                            //activeColor: Colors.pink[300],
                            dense: true,
                            subtitle: Text(showList[index].user.name +
                                " " +
                                showList[index].user.surname),
                            //font change
                            title: new Text(
                              "${showList[index].id}",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 0.5),
                            ),
                            leading: Text("${showList[index].total}" + "£"),
                            onTap: () async {
                              /*var assignResult = await Api
                                      .instance.orderPreparing
                                      .orderPreparingAssignPackagingToMePost(
                                          assignRequest: AssignRequest(
                                              orderId: showList[index].id));*/
                              if (/*assignResult.success*/ true) {
                                await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => WOrderDetails(
                                      orderId: showList[index].id,
                                    ),
                                  ),
                                );
                                load();
                              }
                              /*else {
                                    return showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text(
                                            "Status!",
                                            style: TextStyle(
                                                color: Colors.redAccent),
                                          ),
                                          content: Text(
                                            "The order ${showList[index].id} is busy. Please refresh the page!",
                                          ),
                                          actions: <Widget>[
                                            new TextButton(
                                              child: new Text(
                                                "OK",
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  }*/
                            },
                          )
                        ],
                      ),
                    ),
                  );
                })
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Icon(
                      Icons.shopping_cart_rounded,
                      color: kYellowColor,
                    ),
                  ),
                  Text(
                    "You have no active order right now", /*style: ,*/
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ));
  }
}
