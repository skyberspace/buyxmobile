import 'package:flutter/material.dart';

Color kBackGroundColor = Color(0xffF5F5F5);
Color kPurpleColor = Color(0xff5d3ebd);
Color kYellowColor = Color(0xffffd101);
Color kGreyColor = Color(0xffa2a2a2);
Color kSecondPurpleColor = Color(0xff784AF7);
Color kBrightPurpleColor = Color(0xffF3F0FE);
