import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyx.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/logInAndSignUp/view/LogIn/logIn.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressPage.dart';
import 'package:getir_app/profilePage/view/widgets/changeInformation/changeInformation.dart';
import 'package:getir_app/profilePage/view/widgets/favoriteItems/favoriteItemsPage.dart';
import 'package:getir_app/profilePage/view/widgets/lastOrders/lastOrders.dart';
import 'package:getir_app/profilePage/view/widgets/passwordChange/passwordChange.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/paymentMethods.dart';
import 'package:provider/provider.dart';
import 'widgets/photoAndName/PhotoAndName.dart';
import 'widgets/profilePageIconRow.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "Profil",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: loggedState.checkLoggedIn()
            ? SingleChildScrollView(
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Stack(
                          children: [
                            Card(
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 5, bottom: 5),
                                    child: PhotoAndName(
                                      givenName: "Umut Yeşildal",
                                    ),
                                  ),
                                  Divider(),
                                  ProfilePageIconRow(
                                    givenIcon: Icon(
                                      Icons.mail,
                                      color: kPurpleColor,
                                    ),
                                    givenText: "umutyesid@gmail.com",
                                  ),
                                  Divider(),
                                  ProfilePageIconRow(
                                    givenIcon: Icon(
                                      Icons.phone,
                                      color: kPurpleColor,
                                    ),
                                    givenText: "055555555",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ChangeInformation()));
                                },
                                child: Card(
                                  elevation: 8,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(Icons.edit),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Card(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AdressPage()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.location_on,
                                  color: kPurpleColor,
                                ),
                                givenText: "Adress",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FavoriteItemsPage()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.favorite,
                                  color: kPurpleColor,
                                ),
                                givenText: "Favori ürünler",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PaymentMethods()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.credit_card,
                                  color: kPurpleColor,
                                ),
                                givenText: "Ödeme Yöntemleri",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => lastOrders()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.shopping_bag,
                                  color: kPurpleColor,
                                ),
                                givenText: "Geçmiş Siparişler",
                              ),
                            ),
                            Divider(),
                            ProfilePageIconRow(
                              givenIcon: Icon(
                                Icons.receipt,
                                color: kPurpleColor,
                              ),
                              givenText: "Fatura Bilgilerim",
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PasswordChangePage()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.lock,
                                  color: kPurpleColor,
                                ),
                                givenText: "Şifremi Değiştir",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                loggedState.deleteUser();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NavigationBuyx()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.sensor_door,
                                  color: kPurpleColor,
                                ),
                                givenText: "Çıkış Yap",
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Dil Language",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.language,
                            color: kPurpleColor,
                          ),
                          givenText: "Türkçe",
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Versiyon",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.verified,
                            color: kPurpleColor,
                          ),
                          givenText: "Versiyon",
                        ),
                      )
                    ],
                  ),
                ),
              )
            : SingleChildScrollView(
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20,
                        ),
                        child: Card(
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LogInPage()));
                                },
                                child: ProfilePageIconRow(
                                  givenIcon: Icon(
                                    Icons.person,
                                    color: kPurpleColor,
                                  ),
                                  givenText: "Giriş Yap",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Dil Language",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.language,
                            color: kPurpleColor,
                          ),
                          givenText: "Türkçe",
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Versiyon",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.verified,
                            color: kPurpleColor,
                          ),
                          givenText: "Versiyon",
                        ),
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
