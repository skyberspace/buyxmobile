import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class ProfilePageIconRow extends StatelessWidget {
  const ProfilePageIconRow({this.givenText, this.givenIcon});
  final String givenText;
  final Icon givenIcon;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        children: [
          Expanded(flex: 2, child: givenIcon),
          Expanded(
            flex: 8,
            child: Text(
              givenText,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
