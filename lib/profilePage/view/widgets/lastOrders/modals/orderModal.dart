import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/modals/cardModal.dart';

class orderModal {
  String orderDate;
  String orderAdress;
  String courrierName;
  double orderPrice;
  int orderStar;
  List<itemModal> items;
  cardModal orderUsedCard;
  campaignModal orderCampaign;
//TODO: buraya arkaplan fotoğrafı variable'ı da gelecek.
  orderModal(
      {@required this.orderDate,
      this.courrierName,
      this.orderAdress,
      this.orderPrice,
      this.items,
      this.orderStar,
      this.orderCampaign,
      this.orderUsedCard});
}
