import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/ReusableCard.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/profilePage/view/widgets/lastOrders/modals/orderModal.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/cardRow.dart';

import '../../../../constants.dart';

class OrderDetails extends StatelessWidget {
  OrderDetails({@required this.order});

  final orderModal order;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Sipariş Detayı",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(Icons.home),
                            )),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            order.orderDate,
                            style: TextStyle(
                              color: kSecondPurpleColor,
                            ),
                          ),
                          Text(
                            order.orderAdress,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: kPurpleColor,
                              borderRadius: BorderRadius.circular(4)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(Icons.do_not_touch_outlined,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Kurye",
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            width: double.infinity,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          order.courrierName,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 7,
                        child: SizedBox(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Buyx Deneyiminiz Nasıldı?",
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Row(
                        children: [
                          Icon(
                            Icons.star,
                            color: kGreyColor,
                          ),
                          Icon(
                            Icons.star,
                            color: kGreyColor,
                          ),
                          Icon(
                            Icons.star,
                            color: kGreyColor,
                          ),
                          Icon(
                            Icons.star,
                            color: kGreyColor,
                          ),
                          Icon(
                            Icons.star,
                            color: kGreyColor,
                          ),
                        ],
                      )),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Sepet",
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20),
                  child: Column(
                    children: [
                      ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        primary: true,
                        itemCount: order.items.length,
                        itemBuilder: (context, index) {
                          return ItemRow(item: order.items[index]);
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Ödeme Detayları",
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Material(
              elevation: 10,
              child: Padding(
                padding:
                    EdgeInsets.only(top: 10, left: 20, bottom: 10, right: 10),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sepet Tutarı',
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            order.orderPrice.toString(),
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Getirmesi',
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '4.99',
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Poşet Ücreti (1)',
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '0.25',
                            style: TextStyle(
                                color: kGreyColor, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Ödenecek Tutar',
                            style: TextStyle(
                                color: kPurpleColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            (order.orderPrice + 4.99 + 0.25).toStringAsFixed(2),
                            style: TextStyle(
                                color: kPurpleColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        children: [
                          Icon(Icons.credit_card),
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                order.orderUsedCard.cardName.toUpperCase(),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                order.orderUsedCard.cardNumber.substring(0, 6) +
                                    '*******',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Kullanılan Kampanya",
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ReusableCard(
              header: 'Sıradaki siparişine özel 20 TL hediye!',
              subHeader:
                  'Sıradaki siparişinze özel, 30TL ve üzeri siparişinize 20 tl indirim uygulanır.',
            )
          ],
        ),
      ),
    );
  }
}

class ItemRow extends StatelessWidget {
  const ItemRow({
    Key key,
    @required this.item,
  }) : super(key: key);

  final itemModal item;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: AspectRatio(
            aspectRatio: 1,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 2, color: kBrightPurpleColor),
                borderRadius: BorderRadius.circular(8),
              ),
              width: double.infinity,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(),
        ),
        Expanded(
          flex: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.priceItem.toString(),
                style: TextStyle(
                  color: kPurpleColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                item.nameItem,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                item.descriptionItem,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: SizedBox(),
        ),
        Expanded(
          flex: 2,
          child: AspectRatio(
            aspectRatio: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: kBrightPurpleColor,
                ),
                child: Center(
                  child: Text(
                    item.numberOfItem.toString(),
                    style: TextStyle(
                        color: kPurpleColor, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
