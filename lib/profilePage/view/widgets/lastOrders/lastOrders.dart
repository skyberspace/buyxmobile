import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/profilePage/view/widgets/lastOrders/modals/orderModal.dart';
import 'package:getir_app/profilePage/view/widgets/lastOrders/orderDetails.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/modals/cardModal.dart';

import '../../../../constants.dart';

class lastOrders extends StatelessWidget {
  List<orderModal> orders = [
    orderModal(
        orderDate: '28 Mar 2021 23.34',
        orderAdress: 'Ev',
        courrierName: 'Fatih',
        orderStar: 0,
        orderPrice: 49.99,
        items: [
          itemModal(
              nameItem: 'Golf',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 5),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 7),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 12),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 1),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 89),
        ],
        orderUsedCard: cardModal(
            cardName: 'Osman Tandoğan',
            cardNumber: '432432543432423432423',
            cardExpirationDate: '08.12'),
        orderCampaign: campaignModal(
          header: 'Buyxde sularda %20 indirim!',
          subHeader:
              'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        )),
    orderModal(
        orderDate: '28 Mar 2021 23.34',
        orderAdress: 'Ev',
        courrierName: 'Fatih',
        orderStar: 5,
        orderPrice: 49.99,
        items: [
          itemModal(
              nameItem: 'Golf',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 5),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 7),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 12),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 1),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 89),
        ],
        orderUsedCard: cardModal(
            cardName: 'Osman Tandoğan',
            cardNumber: '432432543432423432423',
            cardExpirationDate: '08.12'),
        orderCampaign: campaignModal(
          header: 'Buyxde sularda %20 indirim!',
          subHeader:
              'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        )),
    orderModal(
        orderDate: '28 Mar 2021 23.34',
        orderAdress: 'İş',
        courrierName: 'Fatih',
        orderStar: 1,
        orderPrice: 429.99,
        items: [
          itemModal(
              nameItem: 'Golf',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 5),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 7),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 12),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 1),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 89),
        ],
        orderUsedCard: cardModal(
            cardName: 'Osman Tandoğan',
            cardNumber: '432432543432423432423',
            cardExpirationDate: '08.12'),
        orderCampaign: campaignModal(
          header: 'Buyxde sularda %20 indirim!',
          subHeader:
              'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        )),
    orderModal(
        orderDate: '28 Mar 2021 23.34',
        orderAdress: 'İş',
        courrierName: 'Fatih',
        orderStar: 3,
        orderPrice: 43.49,
        items: [
          itemModal(
              nameItem: 'Golf',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 5),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 7),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 12),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 1),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 89),
        ],
        orderUsedCard: cardModal(
            cardName: 'Osman Tandoğan',
            cardNumber: '432432543432423432423',
            cardExpirationDate: '08.12'),
        orderCampaign: campaignModal(
          header: 'Buyxde sularda %20 indirim!',
          subHeader:
              'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        )),
    orderModal(
        orderDate: '28 Mar 2021 23.34',
        orderAdress: 'İş',
        courrierName: 'Fatih',
        orderStar: 4,
        orderPrice: 21.99,
        items: [
          itemModal(
              nameItem: 'Golf',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 5),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 7),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 12),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 1),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml',
              numberOfItem: 89),
        ],
        orderUsedCard: cardModal(
            cardName: 'Osman Tandoğan',
            cardNumber: '432432543432423432423',
            cardExpirationDate: '08.12'),
        orderCampaign: campaignModal(
          header: 'Buyxde sularda %20 indirim!',
          subHeader:
              'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        )),
  ];
  @override
  Widget build(BuildContext context) {
    return orders.isNotEmpty
        ? Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: Colors.white,
              ),
              title: Text(
                "Geçmiş Siparişlerim",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              backgroundColor: kPurpleColor,
              elevation: 0,
            ),
            body: ListView.separated(
              itemCount: orders.length,
              separatorBuilder: (BuildContext context, int index) => Divider(),
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OrderDetails(
                                  order: orders[index],
                                )));
                  },
                  child: OrderRow(
                    orderDate: orders[index].orderDate,
                    orderAdress: orders[index].orderAdress,
                    orderPrice: orders[index].orderPrice.toString(),
                  ),
                );
              },
            ),
          )
        : Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: Colors.white,
              ),
              title: Text(
                "Geçmiş Siparişlerim",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              backgroundColor: kPurpleColor,
              elevation: 0,
            ),
            body: Center(
              child: Text('Daha önce verdiğin hiç sipariş yok.'),
            ),
          );
  }
}

class OrderRow extends StatelessWidget {
  const OrderRow({
    @required this.orderAdress,
    this.orderDate,
    this.orderPrice,
  });

  final String orderDate;
  final String orderAdress;
  final String orderPrice;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 4, top: 4, bottom: 4),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(Icons.home),
                )),
          ),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  orderDate,
                  style: TextStyle(
                    color: kSecondPurpleColor,
                  ),
                ),
                Text(
                  orderAdress,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(4),
                          topLeft: Radius.circular(4)),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.shopping_basket,
                        color: kPurpleColor,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(4),
                          topRight: Radius.circular(4)),
                      color: kBrightPurpleColor,
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(4),
                      child: Center(
                        child: Text(
                          orderPrice,
                          style: TextStyle(color: kPurpleColor),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              flex: 1,
              child: Icon(
                Icons.arrow_forward_ios,
                size: 14,
              )),
        ],
      ),
    );
  }
}
