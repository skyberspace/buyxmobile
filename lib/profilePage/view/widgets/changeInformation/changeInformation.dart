import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/profilePage/view/widgets/photoAndName/PhotoAndName.dart';

import '../profilePageIconRow.dart';

class ChangeInformation extends StatefulWidget {
  @override
  _ChangeInformationState createState() => _ChangeInformationState();
}

class _ChangeInformationState extends State<ChangeInformation> {
  final controllerMail = TextEditingController(text: "umutyesildal@gmail.com");
  final controllerName = TextEditingController(text: "Umut Yesil");
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Bilgilerini Düzenle",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: Form(
        key: _formKey,
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.only(top: 5, bottom: 5),
                child: Container(
                  padding: EdgeInsets.only(left: 15, top: 10, right: 10),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 7.2, // 52
                        height: MediaQuery.of(context).size.height / 15.6, // 52
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                            /*  image: DecorationImage(
        image: AssetImage("images/facebookSymbol.png"),
        fit: BoxFit.fill),*/
                            color: Colors.black),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.4,
                        child: TextFormField(
                          controller: controllerName,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                          cursorColor: Colors.black,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Icon(
                          Icons.mail,
                          color: kPurpleColor,
                        )),
                    Expanded(
                      flex: 8,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: controllerMail,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Cant be empty';
                          } else if (!RegExp(
                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                              .hasMatch(value)) {
                            return 'Please enter a valid E-mail';
                          } else {
                            return null;
                          }
                        },
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                        cursorColor: Colors.black,
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Icon(
                          Icons.phone,
                          color: kPurpleColor,
                        )),
                    Expanded(
                      flex: 8,
                      child: Text(
                        '5555555555555',
                        style: TextStyle(
                          color: kGreyColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                  style: ButtonStyle(
                      textStyle: MaterialStateProperty.all<TextStyle>(
                          TextStyle(color: Colors.white)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kPurpleColor)),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text('Kaydet')),
            ],
          ),
        ),
      ),
    );
  }
}
