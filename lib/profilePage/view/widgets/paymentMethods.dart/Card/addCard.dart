import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class AddCard extends StatefulWidget {
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {
  bool checkedValue = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Kart Ekle",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 3.6, // 52
                      height: MediaQuery.of(context).size.height / 7.8, // 52
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Güvenlik",
                            style: TextStyle(
                              color: kPurpleColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                              "Ödeme altyapımız MasterCard uygulaması olan MasterPass tarafından sağlanmaktadır ve işlem güvenliğimiz MasterCard güvencesi altındadır.")
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(12),
                child: TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      helperText: '',
                      hintText: "Karta İsim Ver (Kişisel, İş vb.",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                          ))),
                ),
              ),
              Container(
                padding: EdgeInsets.all(12),
                child: TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Cant be empty';
                    } else if (!RegExp(r'[0-9]').hasMatch(value)) {
                      return 'Please enter a valid Card';
                    } else {
                      return null;
                    }
                  },
                  decoration: InputDecoration(
                      helperText: '',
                      hintText: "Kart Numarası",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                          ))),
                ),
              ),
              Container(
                margin: EdgeInsets.all(12),
                child: TextFormField(
                  keyboardType: TextInputType.datetime,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    helperText: '',
                    hintText: "Kartın Son Kullanım Tarihi",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                ),
              ),
              CheckboxListTile(
                checkColor: Colors.white,
                activeColor: kPurpleColor,
                title: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Kullanım Koşullarını",
                        style: TextStyle(color: kPurpleColor, fontSize: 14),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => print('Tap Here onTap'),
                      ),
                      TextSpan(
                          text: "'nı okudum ve kabul ediyorum.",
                          style: TextStyle(color: Colors.black, fontSize: 14)),
                    ],
                  ),
                ),
                value: checkedValue,
                onChanged: (newValue) {
                  setState(() {
                    checkedValue = newValue;
                  });
                  print(checkedValue);
                },
                controlAffinity:
                    ListTileControlAffinity.leading, //  <-- leading Checkbox
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kPurpleColor),
                    ),
                    onPressed: () {
                      _formKey.currentState.validate();
                    },
                    child: Text(
                      "Devam",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
