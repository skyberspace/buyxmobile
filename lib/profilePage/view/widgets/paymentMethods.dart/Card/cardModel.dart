import 'package:flutter/material.dart';

class AddedCard {
  String name;
  String cardNumber;
  String cardExpirationDate;

  AddedCard({
    @required this.name,
    @required this.cardExpirationDate,
    @required this.cardNumber,
  });
}
