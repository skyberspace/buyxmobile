import 'package:flutter/material.dart';

class CardRow extends StatelessWidget {
  const CardRow({
    @required this.givenCardNumber,
    this.givenName,
  });

  final String givenName;
  final String givenCardNumber;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(flex: 2, child: Icon(Icons.credit_card)),
                Expanded(
                  flex: 8,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        givenName.toUpperCase(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        givenCardNumber.substring(0, 6) +
                            '*******' +
                            givenCardNumber.substring(14, 16),
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
