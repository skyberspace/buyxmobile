import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/addCard.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/cardModel.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/Card/cardRow.dart';

class PaymentMethods extends StatelessWidget {
  List<AddedCard> addedCards = [
    AddedCard(
        name: "Adem Büyük",
        cardExpirationDate: "07/22",
        cardNumber: "3958048237895903"),
    AddedCard(
        name: "Adem kısa",
        cardExpirationDate: "03/20",
        cardNumber: "18569874458998345"),
    AddedCard(
        name: "Adem Büyük",
        cardExpirationDate: "04/22",
        cardNumber: "8573970318569423232"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Ödeme Yöntemleri",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: addedCards.length,
              itemBuilder: (context, index) {
                return CardRow(
                  givenName: addedCards[index].name,
                  givenCardNumber: addedCards[index].cardNumber,
                );
              },
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddCard()));
              },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            child: Center(
                              child: Card(
                                elevation: 5,
                                child: Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 8,
                          child: Text("Kart Ekle",
                              style: TextStyle(
                                color: kPurpleColor,
                                fontWeight: FontWeight.bold,
                              )),
                        )
                      ],
                    ),
                    Divider(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
