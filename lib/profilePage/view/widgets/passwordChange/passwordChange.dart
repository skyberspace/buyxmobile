import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class PasswordChangePage extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Şifremi Değiştir",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Cant be Empty';
                      } else if (value.length <= 4)
                        return 'Cant be shorter than 5 characters';
                    },
                    decoration: InputDecoration(
                        helperText: '',
                        hintText: "Eski Şifreniz",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              style: BorderStyle.solid,
                            ))),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Cant be Empty';
                      } else if (value.length <= 4)
                        return 'Cant be shorter than 5 characters';
                    },
                    decoration: InputDecoration(
                      helperText: '',
                      hintText: "Yeni Şifre (en az 4 Karakter)",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          style: BorderStyle.solid,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(kPurpleColor),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text(
                        "Kaydet",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
