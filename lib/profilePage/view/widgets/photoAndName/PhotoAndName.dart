import 'package:flutter/material.dart';

class PhotoAndName extends StatelessWidget {
  const PhotoAndName({
    @required this.givenName,
  });

  final String givenName;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, top: 10, right: 10),
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 7.2, // 52
            height: MediaQuery.of(context).size.height / 15.6, // 52
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                /*  image: DecorationImage(
      image: AssetImage("images/facebookSymbol.png"),
      fit: BoxFit.fill),*/
                color: Colors.black),
          ),
          SizedBox(
            width: 20,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.4,
            child: Text(
              givenName,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
