import 'package:flutter/material.dart';

class AdressModal {
  Icon icon;
  String adressHeader;
  String adress;
  int binaNo;
  int katNo;
  int daireNo;
  String adressDescription;

  AdressModal(
      {this.icon,
      this.adressHeader,
      this.adress,
      this.binaNo,
      this.katNo,
      this.daireNo,
      this.adressDescription});
}
