import 'package:flutter/material.dart';

class adressModal {
  String header;
  String address;
  String apartmentNo;
  int levelNumber;
  int no;
  String adressDescription;
  adressModal(
      {@required this.header,
      this.address,
      this.apartmentNo,
      this.levelNumber,
      this.no,
      this.adressDescription});
}
