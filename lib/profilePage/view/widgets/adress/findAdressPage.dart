import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/profilePage/view/widgets/adress/addAdressPage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class findAdressPage extends StatefulWidget {
  @override
  _findAdressPageState createState() => _findAdressPageState();
}

class _findAdressPageState extends State<findAdressPage> {
  double longtitudeData = 0;
  double latitudeData = 0;
  String currentAdress = '';
  bool isDone = false;
  @override
  void initState() {
    super.initState();
    _getCurrentLocation().then((value) =>
        _getCurrentAdress(Coordinates(latitudeData, longtitudeData)));
    print(isDone);
  }

  Future _getCurrentLocation() async {
    final geoPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        longtitudeData = geoPosition.longitude;
        latitudeData = geoPosition.latitude;
      });
      print(longtitudeData);
      print(latitudeData);
    });
  }

  _getCurrentAdress(Coordinates coordinates) async {
    final adress =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    Future.delayed(Duration(seconds: 2), () {
      setState(() {
        isDone = true;
        print(adress.first.addressLine);
        currentAdress = adress.first.addressLine;
      });
    });
  }

  Set<Marker> _markers = {};

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _markers.add(Marker(
          draggable: true,
          markerId: MarkerId('id1'),
          position: LatLng(latitudeData, longtitudeData),
          infoWindow: InfoWindow(title: 'Your Current Location'),
          onDragEnd: ((newPosition) {
            setState(() {
              _getCurrentAdress(
                  Coordinates(newPosition.latitude, newPosition.longitude));
            });
          })));
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isDone == false) {
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    } else {
      if (currentAdress == 'North Atlantic Ocean') {
        _getCurrentAdress(Coordinates(latitudeData, longtitudeData));
      }
      return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            title: Text(
              "Yeni Adres Ekle",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            backgroundColor: kPurpleColor,
            elevation: 0,
          ),
          body: Stack(
            children: [
              GoogleMap(
                onMapCreated: _onMapCreated,
                markers: _markers,
                initialCameraPosition: CameraPosition(
                  target: LatLng(latitudeData, longtitudeData),
                  zoom: 17,
                ),
                myLocationButtonEnabled: false,
              ),
              Center(
                child: Column(
                  children: [
                    Material(
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        margin: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Icon(
                              Icons.edit_location,
                              color: kPurpleColor,
                            ),
                            Expanded(
                              child: TextButton(
                                onPressed: () => _getCurrentAdress(
                                    Coordinates(latitudeData, longtitudeData)),
                                child: Text(
                                  !(currentAdress == 'North Atlantic Ocean')
                                      ? currentAdress
                                      : '',
                                  style: TextStyle(
                                      fontSize: 14, color: kGreyColor),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 10,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20)),
                      margin: EdgeInsets.all(24),
                      child: ElevatedButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(
                                    'Adresi Doğru girdiğinize emin misiniz ?'),
                                content: Text(
                                    "Bir sonraki aşamada adresinizi detaylı olarak gireceksiniz."),
                                actions: <Widget>[
                                  TextButton(
                                    child: Text(
                                      "Hayır",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              kGreyColor),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Evet",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();

                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => AddAdressPage(
                                              adress: currentAdress,
                                            ),
                                          ));
                                    },
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              kPurpleColor),
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                        child: Text('Bu Adresi Kullan'),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(kPurpleColor),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ));
    }
  }
}
