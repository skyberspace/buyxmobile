import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/profilePage/view/widgets/adress/findAdressPage.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressModal.dart';

class AdressPage extends StatelessWidget {
  List<AdressModal> adresses = [
    AdressModal(
      icon: Icon(Icons.home),
      adressHeader: "Ev ",
      adress: "Çiçek sokak mokak falan filan deneme deneme",
      adressDescription: "Çiçek parkı karşısı sarı bina",
      binaNo: 13,
      katNo: 21,
      daireNo: 32,
    ),
    AdressModal(
      icon: Icon(Icons.build),
      adressHeader: "İş ",
      adress: "Çiçek sokak mokak falan filan deneme deneme",
      adressDescription: "Çiçek parkı karşısı sarı bina",
      binaNo: 13,
      katNo: 21,
      daireNo: 32,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Adress",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              elevation: 5,
              child: Container(
                child: Center(
                  child: ListView.separated(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: adresses.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(),
                    itemBuilder: (context, index) {
                      return AdressRow(
                        icon: adresses[index].icon,
                        adress: adresses[index].adress,
                        adressHeader: adresses[index].adressHeader,
                      );
                    },
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Adres Ekle",
                style: TextStyle(color: kGreyColor, fontSize: 16),
              ),
            ),
            Material(
              elevation: 5,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => findAdressPage()));
                      },
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.home,
                                  color: kPurpleColor,
                                ),
                                VerticalDivider(),
                                Text(
                                  'Ev adresi ekle',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                )
                              ],
                            ),
                          ),
                          Divider(),
                          Container(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.business,
                                  color: kPurpleColor,
                                ),
                                VerticalDivider(),
                                Text(
                                  'İş adresi ekle',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                )
                              ],
                            ),
                          ),
                          Divider(),
                          Container(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  color: kPurpleColor,
                                ),
                                VerticalDivider(),
                                Text(
                                  'Diğer adres ekle',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AdressRow extends StatelessWidget {
  const AdressRow({this.icon, this.adress, this.adressHeader});

  final Icon icon;
  final String adressHeader;
  final String adress;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 18,
          child: Row(
            children: [
              IconTheme(
                child: icon,
                data: IconThemeData(
                  color: kPurpleColor,
                ),
              ),
              VerticalDivider(),
              Text(
                adressHeader,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: adress == null
                    ? Text("")
                    : Text(
                        adress.substring(0, 35) + "...",
                        style: TextStyle(color: kGreyColor),
                      ),
              ),
              Icon(
                Icons.delete,
                color: kPurpleColor,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
