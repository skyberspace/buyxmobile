import 'package:getir_app/BuyxFoodPage/modals/FoodModal.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/BuyxFoodPage/modals/restaurantCardModal.dart';

class CartBuyxFoodState with ChangeNotifier {
  RestaurantCardModal _restaurant =
      RestaurantCardModal(restaurantName: '', restaurantPhoto: '');
  bool isRestaurantAdded = false;
  List<FoodModal> _foods = [];
  double sumOfAll = 0;

  List<FoodModal> get foods => _foods;
  RestaurantCardModal get restaurant => _restaurant;

  addFirst(FoodModal food) {
    _foods.insert(_foods.length, food);
    notifyListeners();
  }

  incrementFood(FoodModal food) {
    var indexLength =
        _foods.where((element) => element.foodName == food.foodName);
    print(indexLength.length);
    print('This is Length');
    var index = _foods.indexWhere((item) => item.foodName == food.foodName);
    print(index);
    print('This is position');
    _foods[index].numberOfFood++;
    print(_foods[index].numberOfFood);
    notifyListeners();
  }

  decrementFood(FoodModal food) {
    var indexLength =
        _foods.where((element) => element.foodName == food.foodName);
    print(indexLength.length);
    var index = _foods.indexWhere((item) => item.foodName == food.foodName);
    print(index);
    _foods[index].numberOfFood--;
    print(_foods[index].numberOfFood);
    notifyListeners();
  }

  deleteFood(FoodModal food) {
    var index = _foods.indexWhere((place) => place.foodName == food.foodName);
    print(_foods[index].numberOfFood);
    _foods.removeAt(index);
    notifyListeners();
  }

  bool checkEmpty() {
    if (_foods.isEmpty == true)
      return true;
    else
      return false;
  }

  deleteRestaurant() {
    _restaurant = null;
    isRestaurantAdded = false;
  }

  deleteAll() {
    _foods.clear();
    deleteRestaurant();
    calculateSum();
    _restaurant = null;
    isRestaurantAdded = false;
    notifyListeners();
  }

  bool checkExist() {
    return isRestaurantAdded;
  }

  bool addRestaurant(RestaurantCardModal restaurant) {
    if (!checkExist()) {
      print('Option 1');
      notifyListeners();
      isRestaurantAdded = true;
      _restaurant = restaurant;
      return true;
    } else if (_restaurant != restaurant) {
      print('option 2');
      _restaurant = restaurant;
      isRestaurantAdded = true;
      notifyListeners();
      return true;
    }
  }

  calculateSum() {
    double sum = 0;
    for (int i = 0; i < _foods.length; i++) {
      sum = (sum + _foods[i].foodPrice * _foods[i].numberOfFood);
    }
    sumOfAll = sum;
    notifyListeners();
  }
}
