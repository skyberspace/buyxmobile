import 'package:flutter/material.dart';
import 'package:getir_app/BuyxFoodPage/modals/FoodModal.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class CartRowBuyxFood extends StatelessWidget {
  const CartRowBuyxFood(
      {@required this.itemDescription,
      this.itemName,
      this.itemPrice,
      this.itemCount});

  final String itemName;
  final String itemDescription;
  final double itemPrice;
  final int itemCount;

  @override
  Widget build(BuildContext context) {
    FoodModal food = FoodModal(
        foodName: itemName,
        foodDescription: itemDescription,
        foodPrice: itemPrice,
        numberOfFood: itemCount);
    return Consumer<CartBuyxFoodState>(
      builder: (context, state, widget) => Container(
        padding: EdgeInsets.only(left: 15, top: 10, right: 10),
        child: Row(
          children: [
            Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    itemName,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(itemDescription),
                  Text(
                    (itemCount * itemPrice).toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: kPurpleColor,
                        fontSize: 16),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 4,
              child: GestureDetector(
                onTap: () {
                  state.incrementFood(food);
                },
                child: Container(
                    child: ButtonBar(
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        state.incrementFood(food);
                        state.calculateSum();
                      },
                      child: Text(
                        '+',
                        style: TextStyle(
                            color: kPurpleColor, fontWeight: FontWeight.bold),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(kPurpleColor),
                      ),
                      onPressed: () {},
                      child: Text(
                        itemCount.toString(),
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      onPressed: !(itemCount == 1)
                          ? () {
                              state.decrementFood(food);
                              state.calculateSum();
                            }
                          : () {
                              state.deleteFood(food);
                              state.calculateSum();
                              var bool = state.checkEmpty();
                              if (bool == true) state.deleteRestaurant();
                            },
                      child: Text(
                        '-',
                        style: TextStyle(
                            color: kPurpleColor, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
