import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/BuyxFoodPay.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:getir_app/shoppingCartBuyxFood/view/widgets/cartRowBuyxFood.dart';
import 'package:provider/provider.dart';

class CartPageBuyxFood extends StatefulWidget {
  @override
  _CartPageBuyxFoodState createState() => _CartPageBuyxFoodState();
}

class _CartPageBuyxFoodState extends State<CartPageBuyxFood> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CartBuyxFoodState>(
      builder: (context, state, widget) => Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Sepetim",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  if (state.foods.isNotEmpty) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(
                              'Sepetinizi boşaltmak istediğinizden emin misiniz?'),
                          content: Text(
                              "Sepetinizi boşaltırsanız tüm ürünleriniz kaybolacak."),
                          actions: <Widget>[
                            TextButton(
                              child: Text(
                                "Hayır",
                                style: TextStyle(color: Colors.white),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        kGreyColor),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            TextButton(
                              child: Text(
                                "Evet",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                                state.deleteAll();
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        kPurpleColor),
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  } else {}
                })
          ],
        ),
        body: (state.isRestaurantAdded)
            ? Stack(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15, top: 10, bottom: 10, right: 10),
                        child: (state.isRestaurantAdded)
                            ? Row(
                                children: [
                                  Icon(
                                    Icons.fastfood,
                                    color: kPurpleColor,
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Expanded(
                                    child: Text(
                                      state.restaurant.restaurantName,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox(),
                      ),
                      Expanded(
                        child: Material(
                          elevation: 10,
                          child: ListView.separated(
                              separatorBuilder:
                                  (BuildContext context, int index) => Divider(
                                        thickness: 1,
                                      ),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: state.foods.length,
                              itemBuilder: (BuildContext context, int index) {
                                return CartRowBuyxFood(
                                  itemName: state.foods[index].foodName,
                                  itemDescription:
                                      state.foods[index].foodDescription,
                                  itemPrice: state.foods[index].foodPrice,
                                  itemCount: state.foods[index].numberOfFood,
                                );
                              }),
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: new BoxDecoration(
                        border: new Border.all(
                            width: 0.1,
                            color: Colors
                                .transparent), //color is transparent so that it does nßot blend with the actual color specified
                        color: Colors.white60,
                      ),
                      height: MediaQuery.of(context).size.height / 8,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: 25, bottom: 25, left: 10, right: 10),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 6,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BuyxFoodPay()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kPurpleColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Devam',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Container(
                                color: Colors.white,
                                child: Center(
                                  child: Text(
                                    state.sumOfAll.toString(),
                                    style: TextStyle(
                                        color: kPurpleColor,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      'Sepetine bir şeyler eklemek için restaurant sayfasına gidebilirsin.'),
                ),
              ),
      ),
    );
  }
}
