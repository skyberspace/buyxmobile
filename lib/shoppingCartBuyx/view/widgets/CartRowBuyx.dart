import 'package:flutter/material.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class CartRowBuyx extends StatefulWidget {
  const CartRowBuyx(
      {@required this.itemDescription,
      this.itemName,
      this.itemPrice,
      this.itemCount});

  final String itemName;
  final String itemDescription;
  final double itemPrice;
  final int itemCount;

  @override
  _CartRowBuyxState createState() => _CartRowBuyxState();
}

class _CartRowBuyxState extends State<CartRowBuyx> {
  @override
  Widget build(BuildContext context) {
    itemModal item = itemModal(
        nameItem: widget.itemName,
        descriptionItem: widget.itemDescription,
        priceItem: widget.itemPrice,
        numberOfItem: widget.itemCount);
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Container(
        padding: EdgeInsets.only(left: 15, top: 10, right: 10),
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 7.2, // 52
              height: MediaQuery.of(context).size.height / 15.6, // 52
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  /*  image: DecorationImage(
        image: AssetImage("images/facebookSymbol.png"),
        fit: BoxFit.fill),*/
                  color: Colors.black),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.widget.itemName,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(this.widget.itemDescription),
                  Text(
                    this.widget.itemPrice.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: kPurpleColor,
                        fontSize: 16),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 2,
              child: Container(
                  child: ButtonBar(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      state.incrementItem(item);
                      state.calculateSum();
                    },
                    child: Text(
                      '+',
                      style: TextStyle(
                          color: kPurpleColor, fontWeight: FontWeight.bold),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kPurpleColor),
                    ),
                    onPressed: null,
                    child: Text(
                      this.widget.itemCount.toString(),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: !(this.widget.itemCount == 1)
                        ? () {
                            state.decrementItem(item);
                            state.calculateSum();
                          }
                        : () {
                            setState(() {
                              state.deleteItem(item);
                            });
                          },
                    child: Text(
                      '-',
                      style: TextStyle(
                          color: kPurpleColor, fontWeight: FontWeight.bold),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                  ),
                ],
              )),
            )
          ],
        ),
      ),
    );
  }
}
