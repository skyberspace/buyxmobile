import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:flutter/material.dart';

class CartBuyxState with ChangeNotifier {
  List<itemModal> _items = [];
  double sumOfAll = 0;

  List<itemModal> get items => _items;

  addFirst(itemModal item) {
    _items.insert(0, item);
    notifyListeners();
  }

  incrementItem(itemModal item) {
    var indexLength =
        _items.where((element) => element.nameItem == item.nameItem);
    print(indexLength.length);
    var index = _items.indexWhere((place) => place.nameItem == item.nameItem);
    print(index);
    _items[index].numberOfItem++;
    print(_items[index].numberOfItem);
    notifyListeners();
  }

  decrementItem(itemModal item) {
    var indexLength =
        _items.where((element) => element.nameItem == item.nameItem);
    print(indexLength.length);
    var index = _items.indexWhere((place) => place.nameItem == item.nameItem);
    print(index);
    _items[index].numberOfItem--;
    print(_items[index].numberOfItem);
    notifyListeners();
  }

  bool checkExist(itemModal item) {
    if ((_items.firstWhere((iterating) => iterating.nameItem == item.nameItem,
            orElse: () => null)) !=
        null) {
      return true;
    } else {
      return false;
    }
  }

  int getItemCount(itemModal item) {
    var indexLength =
        _items.where((element) => element.nameItem == item.nameItem);
    print(indexLength.length);
    var index = _items.indexWhere((place) => place.nameItem == item.nameItem);
    print(index);
    print(_items[index].numberOfItem);
    return _items[index].numberOfItem;
  }

  deleteItem(itemModal item) {
    var index = _items.indexWhere((place) => place.nameItem == item.nameItem);
    print(_items[index].numberOfItem);
    _items.removeAt(index);
    notifyListeners();
  }

  deleteAll() {
    _items.clear();
    calculateSum();
    notifyListeners();
  }

  calculateSum() {
    double sum = 0;
    for (int i = 0; i < _items.length; i++) {
      sum = (sum + _items[i].priceItem * _items[i].numberOfItem);
    }
    sumOfAll = sum;
    notifyListeners();
  }
}
