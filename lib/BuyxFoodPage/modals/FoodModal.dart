import 'package:flutter/material.dart';

class FoodModal {
  String foodName;
  String foodDescription;
  double foodPrice;
  int numberOfFood;
  FoodModal(
      {@required this.foodName,
      this.foodDescription,
      this.foodPrice,
      this.numberOfFood});
}
