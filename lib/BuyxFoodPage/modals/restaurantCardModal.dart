import 'package:flutter/material.dart';

class RestaurantCardModal {
  String restaurantPhoto;
  String restaurantName;
  String restaurantType;
  String restaurantClosing;
  String restaurantRating;
  String howManyRating;
  bool isDeal;
  String howMuchDeal;
  bool isRestaurantAvailable;
  String minPriceRestaurant;
  String servingTimeRestaurant;
  bool isBuyxAvailable;
  String minPriceBuyx;
  String servingTimeBuyx;
  RestaurantCardModal(
      {@required this.restaurantName,
      @required this.restaurantPhoto,
      this.restaurantType,
      this.restaurantClosing,
      this.restaurantRating,
      this.howManyRating,
      this.isDeal,
      this.howMuchDeal,
      this.isRestaurantAvailable,
      this.minPriceRestaurant,
      this.servingTimeRestaurant,
      this.isBuyxAvailable,
      this.minPriceBuyx,
      this.servingTimeBuyx});
}
