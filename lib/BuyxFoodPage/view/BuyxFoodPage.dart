import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyx.dart';
import 'package:getir_app/BuyxPage/view/widgets/LocationAndTVS.dart';
import 'package:getir_app/BuyxPage/view/widgets/BuyxButton.dart';
import 'package:getir_app/BuyxFoodPage/modals/cuisineCardModal.dart';
import 'package:getir_app/BuyxFoodPage/modals/restaurantCardModal.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/restaurantCard.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/cuisineCard.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/restaurantDetails.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/restaurantRow.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressPage.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:getir_app/shoppingCartBuyxFood/view/cartBuyxFoodPage.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';

class BuyxFoodPage extends StatefulWidget {
  @override
  _BuyxFoodPageState createState() => _BuyxFoodPageState();
}

class _BuyxFoodPageState extends State<BuyxFoodPage> {
  List<RestaurantCardModal> restaurants = [
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: true,
        howMuchDeal: '%40',
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: true,
        howMuchDeal: '%20',
        isBuyxAvailable: false,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: true,
        howMuchDeal: '%15',
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: false,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Dobby's Burger Place (Maslak Mah.)",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
    RestaurantCardModal(
        restaurantPhoto: 'assets/getirYemekRestaurant.jpeg',
        restaurantName: "Kebapçı",
        restaurantType: 'Burger',
        restaurantClosing: '00.00',
        restaurantRating: "4.3",
        howManyRating: "600",
        isDeal: false,
        isBuyxAvailable: true,
        minPriceBuyx: "40.00",
        servingTimeBuyx: "40-50",
        isRestaurantAvailable: true,
        minPriceRestaurant: "25.00",
        servingTimeRestaurant: "30-40"),
  ];
  bool katalogMode = true;

  List<CuisineCardModal> cuisines = [
    CuisineCardModal(
      givenText: "Döner",
    ),
    CuisineCardModal(
      givenText: "Burger",
    ),
    CuisineCardModal(
      givenText: "Kebap",
    ),
    CuisineCardModal(
      givenText: "Tavuk",
    ),
    CuisineCardModal(
      givenText: "Pide",
    ),
    CuisineCardModal(
      givenText: "Pizza",
    ),
  ];
  List<String> images = [
    'assets/getir1.jpeg',
    'assets/getir2.jpeg',
    'assets/getir3.jpeg',
    'assets/getir4.jpeg'
  ];
  int randomInt = Random().nextInt(20);
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size);
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Buyx",
                      style: TextStyle(color: kYellowColor, fontSize: 30),
                    ),
                    TextSpan(
                        text: "yemek",
                        style: TextStyle(color: Colors.white, fontSize: 30)),
                  ],
                ),
              ),
              Consumer<CartBuyxFoodState>(
                  builder: (context, state, widget) => state.foods.isNotEmpty ==
                          true
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CartPageBuyxFood()));
                          },
                          child: Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(4),
                                        topLeft: Radius.circular(4)),
                                    color: Colors.white,
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.shopping_basket,
                                      color: kPurpleColor,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(4),
                                        topRight: Radius.circular(4)),
                                    color: kBrightPurpleColor,
                                  ),
                                  child: Center(
                                    child: Text(
                                      state.sumOfAll.toString(),
                                      style: TextStyle(color: kPurpleColor),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox()),
            ],
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  CarouselSlider(
                    options: CarouselOptions(
                      viewportFraction: 1,
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: true,
                    ),
                    items: images.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Center(child: Image.asset(i)));
                        },
                      );
                    }).toList(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NavigationBuyx()),
                            (Route<dynamic> route) => false,
                          );
                        },
                        child: BuyxButton(
                          givenText: 'Buyx',
                          isTapped: true,
                        ),
                      ),
                      BuyxButton(
                        givenText: 'BuyxFood',
                        isTapped: false,
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Özel İndirimler",
                      style: TextStyle(color: kGreyColor, fontSize: 16),
                    ),
                  ),
                  Material(
                    elevation: 5,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 2.2,
                      padding: EdgeInsets.all(10),
                      child: ListView.separated(
                        itemCount: restaurants.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(
                          width: 10,
                        ),
                        primary: false,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return RestaurantCard(
                            restaurantPhoto: restaurants[index].restaurantPhoto,
                            restaurantName: restaurants[index].restaurantName,
                            restaurantRating:
                                restaurants[index].restaurantRating,
                            minPriceRestaurant:
                                restaurants[index].minPriceRestaurant,
                            isDeal: restaurants[index].isDeal,
                            minPriceBuyx: restaurants[index].minPriceBuyx,
                            howManyRating: restaurants[index].howManyRating,
                            servingTimeBuyx: restaurants[index].servingTimeBuyx,
                            servingTimeRestaurant:
                                restaurants[index].servingTimeRestaurant,
                          );
                        },
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "En Yenileri Keşfet",
                        style: TextStyle(color: kGreyColor, fontSize: 16),
                      )),
                  Material(
                    elevation: 1,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 2.2,
                      padding: EdgeInsets.all(10),
                      child: ListView.separated(
                        itemCount: restaurants.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(
                          width: 10,
                        ),
                        primary: false,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return RestaurantCard(
                            restaurantPhoto: restaurants[index].restaurantPhoto,
                            restaurantName: restaurants[index].restaurantName,
                            restaurantRating:
                                restaurants[index].restaurantRating,
                            minPriceRestaurant:
                                restaurants[index].minPriceRestaurant,
                            isDeal: restaurants[index].isDeal,
                            minPriceBuyx: restaurants[index].minPriceBuyx,
                            howManyRating: restaurants[index].howManyRating,
                            servingTimeBuyx: restaurants[index].servingTimeBuyx,
                            servingTimeRestaurant:
                                restaurants[index].servingTimeRestaurant,
                          );
                        },
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Mutfaklar",
                        style: TextStyle(color: kGreyColor, fontSize: 16),
                      )),
                  Material(
                    elevation: 1,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 10,
                      padding: EdgeInsets.all(10),
                      child: ListView.separated(
                          itemCount: cuisines.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(
                                width: 10,
                              ),
                          primary: false,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return Cuisines(
                              givenPhoto: 'assets/getirDoner.jpeg',
                              givenText: cuisines[index].givenText,
                            );
                          }),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("En Yenileri Keşfet"),
                        TextButton(
                            onPressed: () {
                              print(katalogMode);
                              setState(() {
                                katalogMode = !katalogMode;
                              });
                              print(katalogMode);
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  WidgetSpan(
                                    child: Icon(Icons.add, color: kPurpleColor),
                                  ),
                                  TextSpan(
                                    text: katalogMode ? " Katalog" : " Liste",
                                    style: TextStyle(color: kPurpleColor),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ),
                  Material(
                    elevation: 1,
                    child: Container(
                      child: ListView.separated(
                        itemCount: restaurants.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(),
                        primary: false,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: katalogMode
                                ? MediaQuery.of(context).size.height / 10
                                : MediaQuery.of(context).size.height / 2.2,
                            padding: EdgeInsets.all(10),
                            child: katalogMode
                                ? GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RestaurantDetails(),
                                              settings: RouteSettings(
                                                  arguments:
                                                      restaurants[index])));
                                    },
                                    child: RestaurantRow(
                                      restaurantName:
                                          restaurants[index].restaurantName,
                                      restaurantRating:
                                          restaurants[index].restaurantRating,
                                      howManyRating:
                                          restaurants[index].howManyRating,
                                      minPriceRestaurant:
                                          restaurants[index].minPriceRestaurant,
                                      servingTimeRestaurant: restaurants[index]
                                          .servingTimeRestaurant,
                                      isBuyx:
                                          restaurants[index].isBuyxAvailable,
                                      isRestaurant: restaurants[index]
                                          .isRestaurantAvailable,
                                      minPriceBuyx:
                                          restaurants[index].minPriceBuyx,
                                      servingTimeBuyx:
                                          restaurants[index].servingTimeBuyx,
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RestaurantDetails(),
                                              settings: RouteSettings(
                                                  arguments:
                                                      restaurants[index])));
                                    },
                                    child: RestaurantCard(
                                      restaurantPhoto:
                                          restaurants[index].restaurantPhoto,
                                      restaurantName:
                                          restaurants[index].restaurantName,
                                      restaurantRating:
                                          restaurants[index].restaurantRating,
                                      isDeal: restaurants[index].isDeal,
                                      minPriceRestaurant:
                                          restaurants[index].minPriceRestaurant,
                                      minPriceBuyx:
                                          restaurants[index].minPriceBuyx,
                                      howManyRating:
                                          restaurants[index].howManyRating,
                                      servingTimeBuyx:
                                          restaurants[index].servingTimeBuyx,
                                      servingTimeRestaurant: restaurants[index]
                                          .servingTimeRestaurant,
                                    ),
                                  ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AdressPage()));
                    },
                    child: LocationAndTVS()),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
