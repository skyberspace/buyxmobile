import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxFoodPage/modals/FoodModal.dart';
import 'package:getir_app/BuyxFoodPage/modals/headerModal.dart';
import 'package:getir_app/BuyxFoodPage/modals/restaurantCardModal.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/foodDetails.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:getir_app/shoppingCartBuyxFood/view/cartBuyxFoodPage.dart';

import 'package:provider/provider.dart';
import 'package:indexed_list_view/indexed_list_view.dart';

class RestaurantDetails extends StatefulWidget {
  @override
  _RestaurantDetailsState createState() => _RestaurantDetailsState();
}

class _RestaurantDetailsState extends State<RestaurantDetails> {
  _onStartScroll(ScrollMetrics metrics) {
    print("Scroll Start");
  }

  _onUpdateScroll(ScrollMetrics metrics) {
    print("Scroll Update");
  }

  _onEndScroll(ScrollMetrics metrics) {
    print("Scroll End");
  }

  var controller = IndexedScrollController(
    initialIndex: 0,
  );
  int tabIndex = 0;
  double appBarHeight = 56;

  List<headerModal> headers = [
    headerModal(headerName: 'En Sevilenler', foodList: [
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Lokumburger Menü',
          foodDescription: 'Lokumburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Çiftlikburger Menü',
          foodDescription:
              'Çiftlikburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
    ]),
    headerModal(headerName: 'Pizzalar', foodList: [
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Lokumburger Menü',
          foodDescription: 'Lokumburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Çiftlikburger Menü',
          foodDescription:
              'Çiftlikburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
    ]),
    headerModal(headerName: 'Patatesler', foodList: [
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Lokumburger Menü',
          foodDescription: 'Lokumburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Çiftlikburger Menü',
          foodDescription:
              'Çiftlikburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
    ]),
    headerModal(headerName: 'Menüler', foodList: [
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Lokumburger Menü',
          foodDescription: 'Lokumburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Çiftlikburger Menü',
          foodDescription:
              'Çiftlikburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
    ]),
    headerModal(headerName: 'Falanlar', foodList: [
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Lokumburger Menü',
          foodDescription: 'Lokumburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Çiftlikburger Menü',
          foodDescription:
              'Çiftlikburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
      FoodModal(
          foodName: 'Cheeseburger Menü',
          foodDescription: 'Cheeseburger (130gr), patates kızartması ve içecek',
          foodPrice: 54.00),
    ]),
  ];

  @override
  Widget build(BuildContext context) {
    final RestaurantCardModal card = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        toolbarHeight: appBarHeight,
        title: (appBarHeight == 56)
            ? Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Text(
                      "Restoran detay",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                      ),
                    ),
                    Consumer<CartBuyxFoodState>(
                        builder: (context, state, widget) => state
                                    .foods.isNotEmpty ==
                                true
                            ? GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CartPageBuyxFood()));
                                },
                                child: Container(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(4),
                                              topLeft: Radius.circular(4)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: Icon(
                                            Icons.shopping_basket,
                                            color: kPurpleColor,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(4),
                                              topRight: Radius.circular(4)),
                                          color: kBrightPurpleColor,
                                        ),
                                        child: Center(
                                          child: Text(
                                            state.sumOfAll.toString(),
                                            style:
                                                TextStyle(color: kPurpleColor),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : SizedBox()),
                  ],
                ),
              )
            : Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.arrow_back_ios),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Text(
                        "Restoran detay",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                      SizedBox()
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 40.0,
                    width: 333.0,
                    child: ListView.separated(
                      itemCount: headers.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          SizedBox(
                        width: 10,
                      ),
                      primary: false,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              tabIndex = index;
                              controller.animateToIndex(index);
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(color: kPurpleColor),
                            child: Text(headers[index].headerName,
                                style: tabIndex == index
                                    ? TextStyle(
                                        fontSize: 18,
                                        shadows: [
                                          Shadow(
                                              color: Colors.white,
                                              offset: Offset(0, -5))
                                        ],
                                        color: Colors.transparent,
                                        decoration: TextDecoration.underline,
                                        decorationColor: Colors.orange,
                                        decorationThickness: 4,
                                      )
                                    : TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      )),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            _onStartScroll(scrollNotification.metrics);
          } else if (scrollNotification is ScrollUpdateNotification) {
            _onUpdateScroll(scrollNotification.metrics);
          } else if (scrollNotification is ScrollEndNotification) {
            _onEndScroll(scrollNotification.metrics);
          }
          print(scrollNotification.metrics.pixels);
          if (scrollNotification.metrics.pixels >
              MediaQuery.of(context).size.height / 2.5) {
            setState(() {
              appBarHeight = 100;
            });
          } else {
            setState(() {
              appBarHeight = 56;
            });
          }
        },
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 5,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                    ),
                  ),
                  Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.favorite_border,
                          color: Colors.white,
                        ),
                      )),
                  card.isDeal
                      ? Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                padding: EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                    color: kYellowColor,
                                    borderRadius: BorderRadius.circular(5)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(
                                      Icons.label,
                                      color: kPurpleColor,
                                    ),
                                    Text(card.howMuchDeal +
                                        ' İndirim - Sınırsız')
                                  ],
                                ),
                              )),
                        )
                      : SizedBox(),
                ],
              ),
              Material(
                elevation: 2,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 2,
                              child: Text(
                                card.restaurantName,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(
                                      Icons.star,
                                      color: kPurpleColor,
                                    ),
                                    Text(card.restaurantRating +
                                        " (" +
                                        card.howManyRating +
                                        "+)"),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              card.restaurantType,
                              style: TextStyle(
                                  color: Colors.black45,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Kapanış ' + card.restaurantClosing,
                              style: TextStyle(
                                  color: Colors.black45,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 5, right: 10, top: 5, bottom: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            !card.isBuyxAvailable
                                ? IsOnlyRestaurant()
                                : IsBuyxAvailable(card: card),
                            SizedBox(
                              height: 20,
                            ),
                            card.isRestaurantAvailable
                                ? IsRestaurantAvailable(card: card)
                                : IsOnlyBuyx(),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Card(
                child: Row(
                  children: [
                    Expanded(flex: 2, child: Icon(Icons.search)),
                    Expanded(
                      flex: 8,
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintText: 'Canınız ne çekiyor?'),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Flexible(
                child: Material(
                  elevation: 2,
                  child: Container(
                    height: 5000,
                    width: MediaQuery.of(context).size.width,
                    child: IndexedListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      minItemCount: 0,
                      controller: controller,
                      itemBuilder: (context, listViewIndex) {
                        return (listViewIndex < 0 ||
                                listViewIndex > headers.length - 1)
                            ? null
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    child: Text(
                                      headers[listViewIndex].headerName,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  ListView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      itemCount: headers[listViewIndex]
                                          .foodList
                                          .length,
                                      itemBuilder: (_, gridViewIndex) =>
                                          FoodRow(
                                            restaurantName: card.restaurantName,
                                            foodName: headers[listViewIndex]
                                                .foodList[gridViewIndex]
                                                .foodName,
                                            foodDescription:
                                                headers[listViewIndex]
                                                    .foodList[gridViewIndex]
                                                    .foodDescription,
                                            foodPrice: headers[listViewIndex]
                                                .foodList[gridViewIndex]
                                                .foodPrice,
                                            restaurant: card,
                                          ))
                                ],
                              );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class IsRestaurantAvailable extends StatelessWidget {
  const IsRestaurantAvailable({
    Key key,
    @required this.card,
  }) : super(key: key);

  final RestaurantCardModal card;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(
                  Icons.restaurant,
                  size: 14,
                  color: Colors.green,
                ),
              ),
              TextSpan(
                text: " Restoran",
                style:
                    TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: ' Getirsin',
                style: TextStyle(
                    color: Colors.black45, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          card.servingTimeRestaurant +
              ' dk  - ' +
              ' Ücretsiz Teslimat' +
              ' -  Min ' +
              card.minPriceRestaurant,
          style: TextStyle(color: Colors.black45, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

class IsBuyxAvailable extends StatelessWidget {
  const IsBuyxAvailable({
    Key key,
    @required this.card,
  }) : super(key: key);

  final RestaurantCardModal card;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(
                  Icons.restaurant,
                  size: 14,
                  color: kPurpleColor,
                ),
              ),
              TextSpan(
                text: " Buyx",
                style:
                    TextStyle(color: kPurpleColor, fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: ' Getirsin',
                style: TextStyle(
                    color: Colors.black45, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          card.servingTimeBuyx +
              ' dk  - ' +
              ' Teslimat 5.90 ' +
              ' -  Min ' +
              card.minPriceRestaurant,
          style: TextStyle(color: Colors.black45, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

class IsOnlyRestaurant extends StatelessWidget {
  const IsOnlyRestaurant({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Icon(Icons.shopping_basket, size: 14),
          ),
          TextSpan(
            text:
                " Restoran bu bölgede sadece kendi kuryesiyle hizmet vermektedir.",
            style:
                TextStyle(color: Colors.black45, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}

class IsOnlyBuyx extends StatelessWidget {
  const IsOnlyBuyx({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Icon(Icons.shopping_basket, size: 14),
          ),
          TextSpan(
            text: " Restoran bu bölgede sadece buyx ile hizmet vermektedir.",
            style:
                TextStyle(color: Colors.black45, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}

class FoodRow extends StatelessWidget {
  const FoodRow(
      {@required this.restaurantName,
      this.foodName,
      this.foodDescription,
      this.foodPrice,
      this.restaurant});
  final String restaurantName;
  final String foodName;
  final String foodDescription;
  final double foodPrice;
  final RestaurantCardModal restaurant;
  @override
  Widget build(BuildContext context) {
    var food = FoodModal(
        foodName: foodName,
        foodDescription: foodDescription,
        foodPrice: foodPrice,
        numberOfFood: 1);
    return Consumer<CartBuyxFoodState>(
      builder: (context, state, widget) => Container(
        child: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => foodDetails(
                    restaurantName: restaurantName,
                    foodName: foodName,
                    foodDescription: foodDescription,
                    foodPrice: foodPrice,
                    foodCount: 1,
                    restaurant: restaurant,
                  ),
                ));
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(foodName,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.6,
                      child: Text(
                        foodDescription,
                        style: TextStyle(
                            color: Colors.black45, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      foodPrice.toString(),
                      style: TextStyle(
                          color: kPurpleColor, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4.6, // 52
                    height: MediaQuery.of(context).size.height / 10.15, // 52
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        /*  image: DecorationImage(
            image: AssetImage("images/facebookSymbol.png"),
            fit: BoxFit.fill),*/
                        color: Colors.black),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
