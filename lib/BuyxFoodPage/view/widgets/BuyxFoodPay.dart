import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/buyxFood/view/BuyxFoodPayCampaigns.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxPage/view/widgets/LocationAndTVS.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressPage.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:provider/provider.dart';

class BuyxFoodPay extends StatefulWidget {
  @override
  _BuyxFoodPayState createState() => _BuyxFoodPayState();
}

class _BuyxFoodPayState extends State<BuyxFoodPay> {
  bool dontRingBell = false;
  bool savePlanet = false;
  bool deliveryMethod;
  int billingOption = -1;
  int servingOption = -1;
  var campaign;
  bool isCampaignOn = false;
  int servingTime = -1;
  DateTime _setDate = DateTime.now();
  String dateTime = 'İleri Tarihli';

  Widget datetimePicker() {
    return CupertinoDatePicker(
      initialDateTime: DateTime.now(),
      onDateTimeChanged: (DateTime newdate) {
        print(newdate);
        setState(() {
          dateTime =
              newdate.hour.toString() + ' : ' + newdate.minute.toString();
        });
      },
      use24hFormat: true,
      maximumDate: new DateTime(2021, 12, 30),
      minimumDate: _setDate,
      minimumYear: 2010,
      maximumYear: 2021,
      minuteInterval: 1,
      mode: CupertinoDatePickerMode.time,
    );
  }

  Future<void> bottomSheet(BuildContext context, Widget child,
      {double height}) {
    return showModalBottomSheet(
        isScrollControlled: false,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(13), topRight: Radius.circular(13))),
        backgroundColor: Colors.white,
        context: context,
        builder: (context) => Container(
            height: height ?? MediaQuery.of(context).size.height / 3,
            child: child));
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CartBuyxFoodState>(builder: (context, state, widget) {
      return Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Ödeme Yap",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Not Ekle',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: 15, bottom: 11, top: 11, right: 15),
                                hintText:
                                    'Sipariş notunuzu buraya yazabilirsiniz.',
                                hintStyle: TextStyle(
                                  color: kGreyColor,
                                ),
                              ),
                            ),
                          ),
                          Divider(),
                          CheckboxListTile(
                            checkColor: Colors.white,
                            activeColor: kPurpleColor,
                            title: Text(
                              "Zili Çalma",
                              style: TextStyle(
                                color: kGreyColor,
                                fontSize: 14,
                              ),
                            ),
                            value: dontRingBell,
                            onChanged: (newValue) {
                              setState(() {
                                dontRingBell = newValue;
                              });
                              print(dontRingBell);
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Çevreyi Koru',
                          style: TextStyle(
                            color: kGreyColor,
                          ),
                        ),
                      ),
                      Material(
                        elevation: 10,
                        child: Container(
                          width: double.infinity,
                          color: Colors.white,
                          child: Column(
                            children: [
                              CheckboxListTile(
                                checkColor: Colors.white,
                                activeColor: kPurpleColor,
                                title: Text(
                                  "Bana servis (plastik çatal,bıçak,peçete) gönderme.",
                                  style: TextStyle(
                                    color: kGreyColor,
                                    fontSize: 14,
                                  ),
                                ),
                                value: savePlanet,
                                onChanged: (newValue) {
                                  setState(() {
                                    savePlanet = newValue;
                                  });
                                  print(savePlanet);
                                },
                                controlAffinity: ListTileControlAffinity
                                    .leading, //  <-- leading Checkbox
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Teslimat Yöntemi',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Material(
                                elevation: 5,
                                child: GestureDetector(
                                  onTap: () {
                                    if (state.restaurant.isBuyxAvailable) {
                                      setState(() {
                                        servingOption = 0;
                                        deliveryMethod = false;
                                      });
                                    } else {
                                      return null;
                                    }
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                    child: servingOption == 0
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: kPurpleColor,
                                            ),
                                          )
                                        : SizedBox(),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child:
                                              Icon(Icons.restaurant, size: 14),
                                        ),
                                        TextSpan(
                                            text: " Buyx",
                                            style: TextStyle(
                                              color: kPurpleColor,
                                              fontWeight: FontWeight.bold,
                                            )),
                                        TextSpan(
                                            text: " getirsin",
                                            style: TextStyle(
                                              color: kGreyColor,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ],
                                    ),
                                  ),
                                  state.restaurant.isBuyxAvailable
                                      ? Text(
                                          state.restaurant.servingTimeBuyx +
                                              '- Ücretsiz Teslimat - Min. ' +
                                              state.restaurant.minPriceBuyx,
                                          style: TextStyle(
                                              color: kGreyColor, fontSize: 12),
                                        )
                                      : Text(
                                          'Buyx şu anda aktif değil.',
                                          style: TextStyle(
                                              color: kGreyColor, fontSize: 12),
                                        )
                                ],
                              ),
                              Expanded(
                                flex: 2,
                                child: SizedBox(),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            children: [
                              Material(
                                elevation: 5,
                                child: GestureDetector(
                                  onTap: () {
                                    if (state
                                        .restaurant.isRestaurantAvailable) {
                                      setState(() {
                                        servingOption = 1;
                                        deliveryMethod = true;
                                      });
                                    } else {
                                      return null;
                                    }
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                    child: servingOption == 1
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: kPurpleColor,
                                            ),
                                          )
                                        : SizedBox(),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child: Icon(
                                            Icons.restaurant,
                                            size: 14,
                                            color: Colors.green,
                                          ),
                                        ),
                                        TextSpan(
                                            text: " Restorant",
                                            style: TextStyle(
                                              color: Colors.green,
                                              fontWeight: FontWeight.bold,
                                            )),
                                        TextSpan(
                                            text: " getirsin",
                                            style: TextStyle(
                                              color: kGreyColor,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ],
                                    ),
                                  ),
                                  state.restaurant.isRestaurantAvailable
                                      ? Text(
                                          state.restaurant
                                                  .servingTimeRestaurant +
                                              '- Ücretsiz Teslimat - Min. ' +
                                              state.restaurant
                                                  .minPriceRestaurant,
                                          style: TextStyle(
                                              color: kGreyColor, fontSize: 12),
                                        )
                                      : Text(
                                          'Restorant şu anda aktif değil.',
                                          style: TextStyle(
                                              color: kGreyColor, fontSize: 12),
                                        )
                                ],
                              ),
                              Expanded(
                                flex: 2,
                                child: SizedBox(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  deliveryMethod == true
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Teslimat Zamanı',
                                style: TextStyle(
                                  color: kGreyColor,
                                ),
                              ),
                            ),
                            Material(
                              elevation: 10,
                              child: Container(
                                padding: EdgeInsets.all(8),
                                width: double.infinity,
                                color: Colors.white,
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Material(
                                          elevation: 5,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                servingTime = 0;
                                              });
                                            },
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                              ),
                                              child: servingTime == 0
                                                  ? Container(
                                                      margin: EdgeInsets.all(4),
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: kPurpleColor,
                                                      ),
                                                    )
                                                  : SizedBox(),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(),
                                        ),
                                        Text('Şimdi gelsin'),
                                        Expanded(
                                          flex: 10,
                                          child: SizedBox(),
                                        ),
                                      ],
                                    ),
                                    Divider(),
                                    Row(
                                      children: [
                                        Material(
                                          elevation: 5,
                                          child: GestureDetector(
                                            onTap: () async {
                                              setState(() {
                                                servingTime = 1;
                                              });
                                              await bottomSheet(
                                                  context, datetimePicker());
                                            },
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                              ),
                                              child: servingTime == 1
                                                  ? Container(
                                                      margin: EdgeInsets.all(4),
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: kPurpleColor,
                                                      ),
                                                    )
                                                  : SizedBox(),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(),
                                        ),
                                        Text(dateTime),
                                        Expanded(
                                          flex: 10,
                                          child: SizedBox(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : SizedBox(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Ödeme Yöntemi',
                          style: TextStyle(
                            color: kGreyColor,
                          ),
                        ),
                      ),
                      Material(
                        elevation: 10,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          width: double.infinity,
                          color: Colors.white,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Material(
                                    elevation: 5,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          billingOption = 0;
                                        });
                                      },
                                      child: Container(
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                        child: billingOption == 0
                                            ? Container(
                                                margin: EdgeInsets.all(4),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: kPurpleColor,
                                                ),
                                              )
                                            : SizedBox(),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: SizedBox(),
                                  ),
                                  Expanded(
                                      flex: 1, child: Icon(Icons.credit_card)),
                                  Expanded(
                                    flex: 1,
                                    child: SizedBox(),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'ADEM BÜYÜK',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        '432432423' + '*******' + '323',
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: SizedBox(),
                                  ),
                                  OutlinedButton(
                                    onPressed: () {},
                                    child: Text(
                                      'Kart Değiştir',
                                      style: TextStyle(
                                          fontSize: 12, color: kPurpleColor),
                                    ),
                                  ),
                                ],
                              ),
                              Divider(),
                              deliveryMethod == true
                                  ? Row(
                                      children: [
                                        Material(
                                          elevation: 5,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                billingOption = 1;
                                              });
                                            },
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                              ),
                                              child: billingOption == 1
                                                  ? Container(
                                                      margin: EdgeInsets.all(4),
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: kPurpleColor,
                                                      ),
                                                    )
                                                  : SizedBox(),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(),
                                        ),
                                        Expanded(
                                          flex: 10,
                                          child: Text(
                                            'Kapıda Ödeme',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Ödeme Özeti',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              campaign = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          BuyxFoodPayCampaigns()));

                              if (campaign.toString().isNotEmpty) {
                                setState(() {
                                  isCampaignOn = true;
                                });
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: !isCampaignOn
                                  ? Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Icon(
                                            Icons.redeem,
                                            color: kPurpleColor,
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Text(
                                              'Kampanya Seçin',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )),
                                        Expanded(
                                          flex: 4,
                                          child: SizedBox(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Icon(Icons.arrow_forward_ios),
                                        )
                                      ],
                                    )
                                  : GestureDetector(
                                      onTap: () async {
                                        campaign = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    BuyxFoodPayCampaigns()));

                                        if (campaign.toString().isNotEmpty) {
                                          setState(() {
                                            isCampaignOn = true;
                                          });
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Icon(
                                              Icons.redeem,
                                              color: kPurpleColor,
                                            ),
                                          ),
                                          Expanded(
                                              flex: 4,
                                              child: Text(
                                                campaign.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                          Expanded(
                                            flex: 1,
                                            child:
                                                Icon(Icons.arrow_forward_ios),
                                          )
                                        ],
                                      ),
                                    ),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Sepet Tutarı',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  state.sumOfAll.toString(),
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          servingOption == 0
                              ? Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Getirmesi',
                                            style: TextStyle(
                                                color: kGreyColor,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            '4.99',
                                            style: TextStyle(
                                                color: kGreyColor,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                )
                              : SizedBox(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Ödenecek Tutar',
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  servingOption == 1
                                      ? (state.sumOfAll).toString()
                                      : (state.sumOfAll + 4.99).toString(),
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 8,
                  ),
                  SizedBox(
                    height: 40,
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AdressPage()));
                },
                child: Container(
                  child: LocationAndTVS(),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: new BoxDecoration(
                  border: new Border.all(
                      width: 0.1,
                      color: Colors
                          .transparent), //color is transparent so that it does nßot blend with the actual color specified
                  color: Colors.white60,
                ),
                height: MediaQuery.of(context).size.height / 8,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 25, bottom: 25, left: 10, right: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: Container(
                          decoration: BoxDecoration(
                            color: kPurpleColor,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: Text(
                              'Sipariş ver',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
