import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class RestaurantCard extends StatelessWidget {
  const RestaurantCard({
    @required this.howManyRating,
    this.minPriceBuyx,
    this.restaurantPhoto,
    this.isDeal,
    this.minPriceRestaurant,
    this.restaurantName,
    this.restaurantRating,
    this.servingTimeBuyx,
    this.servingTimeRestaurant,
  });
  final String restaurantPhoto;
  final String restaurantRating;
  final bool isDeal;
  final String howManyRating;
  final String restaurantName;
  final String servingTimeRestaurant;
  final String minPriceRestaurant;
  final String servingTimeBuyx;
  final String minPriceBuyx;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.1,
      height: MediaQuery.of(context).size.height / 2.5,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 8,
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blue,
                    image: DecorationImage(
                      image: AssetImage(restaurantPhoto),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.favorite_border,
                        color: Colors.white,
                      ),
                    )),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: !isDeal
                        ? Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                color: kYellowColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.label,
                                  color: kPurpleColor,
                                ),
                                Text("%40 indirim - Sınırsız")
                              ],
                            ),
                          )
                        : Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: kPurpleColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Yeni",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.star,
                              color: kPurpleColor,
                            ),
                            Text(
                                restaurantRating + " (" + howManyRating + "+)"),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(),
              child: Text(
                restaurantName,
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.restaurant,
                                size: 12,
                                color: Colors.green,
                              ),
                            ),
                            TextSpan(
                              text: " Restoran",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                              text: ' Getirsin',
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black45,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        servingTimeRestaurant +
                            ' dk  - ' +
                            ' Min ' +
                            minPriceRestaurant,
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.black45,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.restaurant,
                                size: 12,
                                color: kPurpleColor,
                              ),
                            ),
                            TextSpan(
                              text: " Buyx",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: kPurpleColor,
                                  fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                              text: ' Getirsin',
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black45,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        servingTimeRestaurant +
                            ' dk  - ' +
                            ' Min ' +
                            minPriceRestaurant,
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.black45,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
