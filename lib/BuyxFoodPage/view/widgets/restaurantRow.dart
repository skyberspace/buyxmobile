import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class RestaurantRow extends StatelessWidget {
  const RestaurantRow({
    @required this.restaurantName,
    this.isBuyx,
    this.isRestaurant,
    this.servingTimeRestaurant,
    this.howManyRating,
    this.minPriceRestaurant,
    this.minPriceBuyx,
    this.servingTimeBuyx,
    this.restaurantRating,
  });

  final String restaurantName;
  final String servingTimeRestaurant;
  final String minPriceRestaurant;
  final String restaurantRating;
  final String howManyRating;
  final bool isBuyx;
  final bool isRestaurant;
  final String minPriceBuyx;
  final String servingTimeBuyx;

  bool isLongerThan15(String givenText) {
    if (givenText.length > 15)
      return true;
    else
      return false;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: MediaQuery.of(context).size.width / 7.2, // 52
          height: MediaQuery.of(context).size.height / 15.6, // 52
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              /*  image: DecorationImage(
      image: AssetImage("images/facebookSymbol.png"),
      fit: BoxFit.fill),*/
              color: Colors.black),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              (isLongerThan15(restaurantName))
                  ? restaurantName.substring(0, 15)
                  : restaurantName,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            isBuyx
                ? Row(
                    children: [
                      Icon(
                        Icons.restaurant,
                        color: kPurpleColor,
                        size: 12,
                      ),
                      Text(
                        servingTimeBuyx + ' dk - Min ' + minPriceBuyx,
                        style: TextStyle(color: kGreyColor, fontSize: 12),
                      ),
                    ],
                  )
                : SizedBox(),
            isRestaurant
                ? Row(
                    children: [
                      Icon(
                        Icons.restaurant,
                        color: Colors.green,
                        size: 12,
                      ),
                      Text(
                        servingTimeRestaurant +
                            ' dk - Min ' +
                            minPriceRestaurant,
                        style: TextStyle(color: kGreyColor, fontSize: 12),
                      ),
                    ],
                  )
                : SizedBox(),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Card(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.star, color: kPurpleColor),
                  Text(restaurantRating + ' (' + howManyRating + '+)'),
                ],
              ),
            ),
            Expanded(
              child: Icon(
                Icons.favorite_border,
                color: Colors.black,
              ),
            )
          ],
        )
      ],
    );
  }
}
