import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxFoodPage/modals/FoodModal.dart';
import 'package:getir_app/BuyxFoodPage/modals/restaurantCardModal.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:provider/provider.dart';

class foodDetails extends StatefulWidget {
  foodDetails(
      {@required this.foodName,
      this.restaurantName,
      this.foodDescription,
      this.foodPrice,
      this.foodCount,
      @required this.restaurant});
  final String foodName;
  final String foodDescription;
  final double foodPrice;
  final int foodCount;
  final String restaurantName;
  final RestaurantCardModal restaurant;

  @override
  _foodDetailsState createState() => _foodDetailsState();
}

class _foodDetailsState extends State<foodDetails> {
  int foodCount = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Yemek Detayı",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
              ),
              Card(
                child: Column(
                  children: [
                    Text(
                      widget.foodName,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      widget.foodDescription,
                      style: TextStyle(color: kGreyColor),
                    ),
                    Text(
                      widget.foodPrice.toString(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: kPurpleColor,
                          fontSize: 20),
                    ),
                  ],
                ),
              ),
              ButtonBar(
                mainAxisSize: MainAxisSize
                    .min, // this will take space as minimum as posible(to center)
                children: <Widget>[
                  new ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    child: new Text(
                      '-',
                      style: TextStyle(color: kPurpleColor, fontSize: 16),
                    ),
                    onPressed: () {
                      setState(() {
                        if (foodCount == 1) {
                        } else {
                          foodCount--;
                        }
                      });
                    },
                  ),
                  new ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(kPurpleColor)),
                    child: new Text(
                      foodCount.toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: null,
                  ),
                  new ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.white)),
                    child: new Text(
                      '+',
                      style: TextStyle(color: kPurpleColor, fontSize: 16),
                    ),
                    onPressed: () {
                      setState(() {
                        foodCount++;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: AddFoodRow(
                restaurantName: widget.restaurantName,
                foodName: widget.foodName,
                foodDescription: widget.foodDescription,
                foodCount: foodCount,
                foodPrice: widget.foodPrice,
                restaurant: widget.restaurant),
          )
        ],
      ),
    );
  }
}

class AddFoodRow extends StatelessWidget {
  const AddFoodRow(
      {@required this.foodCount,
      this.restaurantName,
      this.foodDescription,
      this.foodName,
      this.foodPrice,
      this.restaurant});
  final String restaurantName;
  final int foodCount;
  final double foodPrice;
  final String foodName;
  final String foodDescription;
  final RestaurantCardModal restaurant;

  @override
  Widget build(BuildContext context) {
    print(foodCount);
    var food = FoodModal(
        foodName: foodName,
        foodDescription: foodDescription,
        foodPrice: foodPrice,
        numberOfFood: foodCount);
    return Container(
      decoration: new BoxDecoration(
        border: new Border.all(width: 0.1, color: Colors.transparent),
        color: Colors.white60,
      ),
      height: MediaQuery.of(context).size.height / 8,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.only(top: 25, bottom: 25, left: 10, right: 10),
        child: Row(
          children: [
            Expanded(
              flex: 6,
              child: Consumer<CartBuyxFoodState>(
                builder: (context, state, widget) => GestureDetector(
                  onTap: () async {
                    if (state.checkExist() && state.restaurant != restaurant) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                                'Sepetinizde farklı bir restauranttan ürün bulunmakta.'),
                            content: Text(
                                "Sepetinize bu ürünü eklemek için sepetinizi boşaltmak ister misiniz?"),
                            actions: <Widget>[
                              TextButton(
                                child: Text(
                                  "Hayır",
                                  style: TextStyle(color: Colors.white),
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kGreyColor),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: Text(
                                  "Evet",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  state.deleteAll();
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kPurpleColor),
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    }
                    state.addFirst(food);
                    state.addRestaurant(restaurant);
                    state.calculateSum();
                    Future.delayed(Duration(milliseconds: 500));
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: kPurpleColor,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Center(
                      child: Text(
                        'Sepete Ekle',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: Text(
                    (foodCount * foodPrice).toString(),
                    style: TextStyle(
                        color: kPurpleColor,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
