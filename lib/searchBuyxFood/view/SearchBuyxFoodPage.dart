import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxFoodPage/modals/cuisineCardModal.dart';
import 'package:getir_app/BuyxFoodPage/view/widgets/cuisineCard.dart';
import 'package:getir_app/searchBuyx/view/modals/popularSearchItemModal.dart';
import 'package:getir_app/searchBuyx/view/widgets/popularSearchItems.dart';
import 'package:getir_app/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:getir_app/shoppingCartBuyxFood/view/cartBuyxFoodPage.dart';
import 'package:provider/provider.dart';

class SearchBuyxFoodPage extends StatefulWidget {
  @override
  _SearchBuyxFoodPageState createState() => _SearchBuyxFoodPageState();
}

class _SearchBuyxFoodPageState extends State<SearchBuyxFoodPage> {
  List<PopularSearchItemModal> items = [
    PopularSearchItemModal(name: "Burger"),
    PopularSearchItemModal(name: "Pizza"),
    PopularSearchItemModal(name: "Baklava"),
    PopularSearchItemModal(name: "Lahmacun"),
    PopularSearchItemModal(name: "Döner"),
    PopularSearchItemModal(name: "su"),
    PopularSearchItemModal(name: "dondurma"),
    PopularSearchItemModal(name: "ekmek"),
    PopularSearchItemModal(name: "sucuk"),
  ];

  List<CuisineCardModal> cuisines = [
    CuisineCardModal(
      givenText: "Döner",
    ),
    CuisineCardModal(
      givenText: "Burger",
    ),
    CuisineCardModal(
      givenText: "Kebap",
    ),
    CuisineCardModal(
      givenText: "Tavuk",
    ),
    CuisineCardModal(
      givenText: "Pide",
    ),
    CuisineCardModal(
      givenText: "Pizza",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              Text(
                "Arama",
                style: TextStyle(color: Colors.white),
              ),
              Consumer<CartBuyxFoodState>(
                  builder: (context, state, widget) => state.foods.isNotEmpty ==
                          true
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CartPageBuyxFood()));
                          },
                          child: Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(4),
                                        topLeft: Radius.circular(4)),
                                    color: Colors.white,
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.shopping_basket,
                                      color: kPurpleColor,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(4),
                                        topRight: Radius.circular(4)),
                                    color: kBrightPurpleColor,
                                  ),
                                  child: Center(
                                    child: Text(
                                      state.sumOfAll.toString(),
                                      style: TextStyle(color: kPurpleColor),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox()),
            ],
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: Column(
        children: [
          Material(
            elevation: 10,
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 13.5,
              color: Colors.white,
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Icon(Icons.search, color: kPurpleColor),
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Ürün Ara",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  Expanded(flex: 1, child: Icon(Icons.mic))
                ],
              ),
            ),
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10, top: 30),
                child: Text(
                  "Arama Geçmişi",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: ListView(
              primary: false,
              shrinkWrap: true,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          'Pide',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          'Pide',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Center(
                            child: Text(
                              'Pide',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10, top: 30),
                child: Text(
                  "Popüler Aramalar",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
          Material(
            elevation: 2,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 10,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Center(
                child: ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return PopularSearchItem(
                      givenText: items[index].name,
                    );
                  },
                ),
              ),
            ),
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10, top: 30),
                child: Text(
                  "Mutfaklar",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
          Material(
            elevation: 1,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 10,
              padding: EdgeInsets.all(10),
              child: ListView.separated(
                  itemCount: cuisines.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      SizedBox(
                        width: 10,
                      ),
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Cuisines(
                      givenText: cuisines[index].givenText,
                      givenPhoto: 'assets/getirDoner.jpeg',
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
