import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyxFood.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxPage/itemTabs/itemTabs.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/headerModal.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/tabModal.dart';
import 'package:getir_app/BuyxPage/view/widgets/BuyxButton.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressPage.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:getir_app/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:provider/provider.dart';
import 'widgets/BuyxCard.dart';
import 'widgets/LocationAndTVS.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:math';

class BuyxPage extends StatefulWidget {
  @override
  _BuyxPageState createState() => _BuyxPageState();

  bool isTappedBuyx = false;
}

class _BuyxPageState extends State<BuyxPage> {
  List<String> images = [
    'assets/getir1.jpeg',
    'assets/getir2.jpeg',
    'assets/getir3.jpeg',
    'assets/getir4.jpeg'
  ];
  List<String> cardTexts = [
    'Dondurma',
    'İndirimler',
    'Damacana',
    'Su İçecek',
    'Yiyecek',
    'Form',
    'Kahvaltı',
    'Teknoloji'
  ];
  List<String> cardImages = [
    'https://cdn.getir.com/cat/56dfcfba86004203000a870d_1587466565958_1587466566031.jpeg',
    'https://cdn.getir.com/cat/56dfe03cf82055030022cdc0_1587466838822_1587466838892.jpeg',
    'https://cdn.getir.com/cat/5928113e616cab00041ec6de_1587465555964_1587465556033.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c5e_1592162100436_1592162100504.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c5c_1610709580199_1610709580548.jpeg',
    'https://cdn.getir.com/cat/55449fdf02632e11003c2da8_1587466341469_1587466341536.jpeg',
    'https://cdn.getir.com/cat/5b06b056b883b700044e3e4c_1590944818129_1590944818244.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c62_1587466209941_1587466210003.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c5c_1610709580199_1610709580548.jpeg',
    'https://cdn.getir.com/cat/55449fdf02632e11003c2da8_1587466341469_1587466341536.jpeg',
    'https://cdn.getir.com/cat/5b06b056b883b700044e3e4c_1590944818129_1590944818244.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c62_1587466209941_1587466210003.jpeg'
        'https://cdn.getir.com/cat/551430043427d5010a3a5c5c_1610709580199_1610709580548.jpeg',
    'https://cdn.getir.com/cat/55449fdf02632e11003c2da8_1587466341469_1587466341536.jpeg',
    'https://cdn.getir.com/cat/5b06b056b883b700044e3e4c_1590944818129_1590944818244.jpeg',
    'https://cdn.getir.com/cat/551430043427d5010a3a5c62_1587466209941_1587466210003.jpeg'
  ];

  T getRandomElement<T>(List<T> list) {
    final random = new Random();
    var i = random.nextInt(list.length);
    return list[i];
  }

  int randomInt = Random().nextInt(20);
  @override
  Widget build(BuildContext context) {
    List<headerModal> headers = [
      headerModal(
        itemList: [
          itemModal(
              nameItem: 'Bounty Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Twix Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Golf', priceItem: 15.50, descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
        ],
        listName: 'Bar',
      ),
      headerModal(listName: 'Royalty Chicken', itemList: [
        itemModal(
            nameItem: 'Fantasia Black',
            priceItem: 15.50,
            descriptionItem: '125 ml'),
        itemModal(
            nameItem: 'Golf Maraışım Dondurma',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Pernigotti',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Cornetto Oreo',
            priceItem: 15.50,
            descriptionItem: '520 ml'),
        itemModal(
            nameItem: 'Cornetto Milka',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Pernigotti karamelli',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
      ])
    ];
    List<headerModal> headers2 = [
      headerModal(
        itemList: [
          itemModal(
              nameItem: 'Bounty Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Twix Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Golf', priceItem: 15.50, descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'WoopieCookie',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Pernigotti',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Nogger Sandwich',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
          itemModal(
              nameItem: 'Algida Dondurma',
              priceItem: 15.50,
              descriptionItem: '50 ml'),
        ],
        listName: 'Bar',
      ),
      headerModal(listName: 'Kingdom', itemList: [
        itemModal(
            nameItem: 'Fantasia Black',
            priceItem: 15.50,
            descriptionItem: '125 ml'),
        itemModal(
            nameItem: 'Golf Maraışım Dondurma',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Pernigotti',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Cornetto Oreo',
            priceItem: 15.50,
            descriptionItem: '520 ml'),
        itemModal(
            nameItem: 'Cornetto Milka',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Pernigotti karamelli',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
      ]),
      headerModal(listName: 'Kebab', itemList: [
        itemModal(
            nameItem: 'Fantasia Black',
            priceItem: 15.50,
            descriptionItem: '125 ml'),
        itemModal(
            nameItem: 'Golf Maraışım Dondurma',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Pernigotti',
            priceItem: 15.50,
            descriptionItem: '120 ml'),
        itemModal(
            nameItem: 'Cornetto Oreo',
            priceItem: 15.50,
            descriptionItem: '520 ml'),
        itemModal(
            nameItem: 'Cornetto Milka',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Pernigotti karamelli',
            priceItem: 15.50,
            descriptionItem: '50 ml'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
        itemModal(
            nameItem: 'Dondurma Külahı',
            priceItem: 15.50,
            descriptionItem: '10 Adet'),
      ]),
    ];
    List<tabModal> tabs = [
      tabModal(nameItem: 'Dondurma', headerList: headers),
      tabModal(nameItem: 'Yiyecek', headerList: headers2),
      tabModal(nameItem: 'İçecek', headerList: headers),
      tabModal(nameItem: 'Cips', headerList: headers),
      tabModal(nameItem: 'Fırından', headerList: headers),
      tabModal(nameItem: 'Kahvaltı', headerList: headers),
      tabModal(nameItem: 'Yemek', headerList: headers),
      tabModal(nameItem: 'Form', headerList: headers),
    ];
    print(MediaQuery.of(context).size);
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              Text(
                "Buyx",
                style: TextStyle(
                  color: kYellowColor,
                  fontSize: 32,
                ),
              ),
              Consumer<CartBuyxState>(
                  builder: (context, state, widget) =>
                      state.items.isNotEmpty == true
                          ? GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CartPageBuyx()));
                              },
                              child: Container(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(4),
                                            topLeft: Radius.circular(4)),
                                        color: Colors.white,
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.shopping_basket,
                                          color: kPurpleColor,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(4),
                                            topRight: Radius.circular(4)),
                                        color: kBrightPurpleColor,
                                      ),
                                      child: Center(
                                        child: Text(
                                          state.sumOfAll.toString(),
                                          style: TextStyle(color: kPurpleColor),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox()),
            ],
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  CarouselSlider(
                    options: CarouselOptions(
                      viewportFraction: 1,
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: true,
                    ),
                    items: images.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Center(child: Image.asset(i)));
                        },
                      );
                    }).toList(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      BuyxButton(
                        givenText: 'Buyx',
                        isTapped: false,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NavigationBuyxFood()),
                            (Route<dynamic> route) => false,
                          );
                        },
                        child: BuyxButton(
                          givenText: 'BuyxFood',
                          isTapped: true,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: GridView.count(
                      physics: new NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      crossAxisCount: 4,
                      children: List.generate(tabs.length, (index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BuyxTabs(
                                    initPos: index,
                                    tabs: tabs,
                                  ),
                                ));
                          },
                          child: BuyxCard(
                            givenText: tabs[index].nameItem,
                            givenImage: cardImages[index],
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
            Consumer<LoggedInState>(
              builder: (context, state, widget) => Align(
                alignment: Alignment.topCenter,
                child: Container(
                  child: GestureDetector(
                      onTap: () {
                        state.checkLoggedIn()
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AdressPage()))
                            : showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Giriş Yapmalısınız.'),
                                    content: Text(
                                        "Adres eklemek için profil kısmından giriş yapmalısınız."),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text(
                                          "Anladım.",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  kGreyColor),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                      },
                      child: LocationAndTVS()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
