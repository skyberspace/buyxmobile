import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/buyx/view/BuyxPayCampaigns.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxPage/view/widgets/LocationAndTVS.dart';
import 'package:getir_app/profilePage/view/widgets/adress/adressPage.dart';
import 'package:getir_app/profilePage/view/widgets/paymentMethods.dart/paymentMethods.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:provider/provider.dart';

class BuyxPay extends StatefulWidget {
  @override
  _BuyxPayState createState() => _BuyxPayState();
}

class _BuyxPayState extends State<BuyxPay> {
  bool dontRingBell = false;
  bool iAgree = false;
  var campaign;
  bool isCampaignOn = false;
  int billingOption = 0;
  @override
  Widget build(BuildContext context) {
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Ödeme Yap",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Not Ekle',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: 15, bottom: 11, top: 11, right: 15),
                                hintText:
                                    'Sipariş notunuzu buraya yazabilirsiniz.',
                                hintStyle: TextStyle(
                                  color: kGreyColor,
                                ),
                              ),
                            ),
                          ),
                          Divider(),
                          CheckboxListTile(
                            checkColor: Colors.white,
                            activeColor: kPurpleColor,
                            title: Text(
                              "Zili Çalma",
                              style: TextStyle(
                                color: kGreyColor,
                                fontSize: 14,
                              ),
                            ),
                            value: dontRingBell,
                            onChanged: (newValue) {
                              setState(() {
                                dontRingBell = newValue;
                              });
                              print(dontRingBell);
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Ödeme Yöntemi',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Material(
                                elevation: 5,
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      billingOption = 0;
                                    });
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                    child: billingOption == 0
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: kPurpleColor,
                                            ),
                                          )
                                        : SizedBox(),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Expanded(flex: 1, child: Icon(Icons.credit_card)),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'ADEM BÜYÜK',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    '432432423' + '*******' + '323',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                              Expanded(
                                flex: 2,
                                child: SizedBox(),
                              ),
                              OutlinedButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              PaymentMethods()));
                                },
                                child: Text(
                                  'Kart Değiştir',
                                  style: TextStyle(
                                      fontSize: 12, color: kPurpleColor),
                                ),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            children: [
                              Material(
                                elevation: 5,
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      billingOption = 1;
                                    });
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                    child: billingOption == 1
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: kPurpleColor,
                                            ),
                                          )
                                        : SizedBox(),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Expanded(flex: 1, child: Icon(Icons.credit_card)),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Text(
                                'İstanbul Kart ile Öde',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: SizedBox(),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Card(
                                  child: Icon(
                                    Icons.add,
                                    color: kPurpleColor,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Text(
                                'BMK Express ile Kart Ekle',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kPurpleColor),
                              ),
                              Expanded(
                                flex: 2,
                                child: SizedBox(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Ödeme Özeti',
                      style: TextStyle(
                        color: kGreyColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              campaign = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          BuyxPayCampaigns()));

                              if (campaign.toString().isNotEmpty) {
                                setState(() {
                                  isCampaignOn = true;
                                });
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: !isCampaignOn
                                  ? Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Icon(
                                            Icons.redeem,
                                            color: kPurpleColor,
                                          ),
                                        ),
                                        Expanded(
                                            flex: 4,
                                            child: Text(
                                              'Kampanya Seçin',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )),
                                        Expanded(
                                          flex: 4,
                                          child: SizedBox(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Icon(Icons.arrow_forward_ios),
                                        )
                                      ],
                                    )
                                  : GestureDetector(
                                      onTap: () async {
                                        campaign = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    BuyxPayCampaigns()));

                                        if (campaign.toString().isNotEmpty) {
                                          setState(() {
                                            isCampaignOn = true;
                                          });
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Icon(
                                              Icons.redeem,
                                              color: kPurpleColor,
                                            ),
                                          ),
                                          Expanded(
                                              flex: 4,
                                              child: Text(
                                                campaign.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                          Expanded(
                                            flex: 1,
                                            child:
                                                Icon(Icons.arrow_forward_ios),
                                          )
                                        ],
                                      ),
                                    ),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Card(
                                      child: Icon(
                                        Icons.add,
                                        color: kPurpleColor,
                                      ),
                                    )),
                                Expanded(
                                    flex: 4,
                                    child: Text(
                                      'Fatura Bilgisi Ekle',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: kPurpleColor),
                                    )),
                                Expanded(
                                  flex: 5,
                                  child: SizedBox(),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Sepet Tutarı',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  state.sumOfAll.toString(),
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Getirmesi',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  '4.99',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Poşet Ücreti (2)',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  '0.50',
                                  style: TextStyle(
                                      color: kGreyColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Ödenecek Tutar',
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  (state.sumOfAll + 0.5 + 4.99).toString(),
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          CheckboxListTile(
                            checkColor: Colors.white,
                            activeColor: kPurpleColor,
                            title: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                      text:
                                          "Ön Bilgilendirme Formu ve Mesafeli Satış Sözleşmesi'",
                                      style: TextStyle(
                                          color: kPurpleColor, fontSize: 12)),
                                  TextSpan(
                                    text: "ni okudum ve kabul ediyorum.",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                            value: iAgree,
                            onChanged: (newValue) {
                              setState(() {
                                iAgree = newValue;
                              });
                              print(iAgree);
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 8,
                  ),
                  SizedBox(
                    height: 40,
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AdressPage()));
                },
                child: Container(
                  child: LocationAndTVS(),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: new BoxDecoration(
                  border: new Border.all(
                      width: 0.1,
                      color: Colors
                          .transparent), //color is transparent so that it does nßot blend with the actual color specified
                  color: Colors.white60,
                ),
                height: MediaQuery.of(context).size.height / 8,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 25, bottom: 25, left: 10, right: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: Container(
                          decoration: BoxDecoration(
                            color: kPurpleColor,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: Text(
                              'Sipariş ver',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
