import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class BuyxButton extends StatefulWidget {
  const BuyxButton({
    @required this.givenText,
    this.isTapped,
  });
  final String givenText;
  final bool isTapped;

  @override
  _BuyxButtonState createState() => _BuyxButtonState();
}

class _BuyxButtonState extends State<BuyxButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        color: widget.isTapped ? Colors.white : kPurpleColor,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            widget.givenText,
            style: TextStyle(
              color: widget.isTapped ? kPurpleColor : kYellowColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
