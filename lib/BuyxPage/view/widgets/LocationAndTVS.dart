import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:provider/provider.dart';

class LocationAndTVS extends StatelessWidget {
  const LocationAndTVS({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, state, widget) => Stack(
        children: [
          Container(
            color: kYellowColor,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 13.5, // 60
            child: Align(
              alignment: Alignment.centerRight,
              child: Container(
                padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width / 12.5,
                  top: 5,
                ), // 30 10
                color: kYellowColor,
                child: Column(
                  children: [
                    Text(
                      "TVS",
                      style: TextStyle(
                        color: kPurpleColor,
                        fontSize: 12,
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: state.checkLoggedIn() ? "9" : '', // 9 TVS
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              color: kPurpleColor,
                            ),
                          ),
                          TextSpan(
                            text: "dk",
                            style: TextStyle(
                              color: kPurpleColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            width: MediaQuery.of(context).size.width / 1.3,
            height: MediaQuery.of(context).size.height / 13.5, // 60
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.zero,
                topRight: Radius.circular(30.0),
                bottomLeft: Radius.zero,
                bottomRight: Radius.circular(30.0),
              ),
            ),
            child: state.checkLoggedIn()
                ? Row(
                    children: [
                      Icon(Icons.home),
                      Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: VerticalDivider()),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Ev ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            TextSpan(
                              text: "Çiçek Sokak Çeliktepe Mah",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 20,
                      ),
                    ],
                  )
                : Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Sipariş vermek için Teslimat Adresi seçmeniz gerekiyor.',
                        ),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 20,
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }
}
