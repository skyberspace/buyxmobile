import 'package:flutter/material.dart';

class BuyxCard extends StatelessWidget {
  const BuyxCard({
    @required this.givenText,
    this.givenImage,
  });
  final String givenText;
  final String givenImage;
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: FadeInImage.assetNetwork(
                  fit: BoxFit.cover,
                  placeholder: 'assets/placeholder.gif',
                  image: givenImage),
            ),
          ),
          Text(
            givenText,
          )
        ],
      ),
    );
  }
}
