import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:provider/provider.dart';

class itemsDetailed extends StatefulWidget {
  itemsDetailed({
    @required this.itemName,
    this.itemDescription,
    this.itemMoreDescription,
    this.itemPrice,
    this.itemCount,
  });

  final String itemName;
  final double itemPrice;
  final String itemDescription;
  final String itemMoreDescription;
  final int itemCount;

  @override
  _itemsDetailedState createState() => _itemsDetailedState();
}

class _itemsDetailedState extends State<itemsDetailed> {
  int itemCount;
  @override
  void initState() {
    itemCount = widget.itemCount;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var item = itemModal(
        priceItem: widget.itemPrice,
        descriptionItem: widget.itemDescription,
        nameItem: widget.itemName,
        numberOfItem: 1);
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Consumer<CartBuyxState>(
        builder: (context, state, widget) => Scaffold(
          backgroundColor: kBackGroundColor,
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context, itemCount);
              },
            ),
            title: Text(
              "Ürün Detayı",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            backgroundColor: kPurpleColor,
            elevation: 0,
            actions: <Widget>[
              Icon(
                Icons.favorite,
              )
            ],
          ),
          body: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Material(
                    elevation: 10,
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            height: 300,
                            width: double.infinity,
                            color: Colors.black,
                          ),
                          Text(
                            this.widget.itemPrice.toString(),
                            style: TextStyle(
                              color: kPurpleColor,
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            this.widget.itemName,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(this.widget.itemDescription),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Açıklama',
                      style: TextStyle(
                          color: kGreyColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    width: double.infinity,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child:
                            Text('Çikolata, karamel ve bisküvinin buluşması'),
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: new BoxDecoration(
                    border: new Border.all(
                        width: 0.1,
                        color: Colors
                            .transparent), //color is transparent so that it does nßot blend with the actual color specified
                    color: Colors.white60,
                  ),
                  height: MediaQuery.of(context).size.height / 8,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 25, bottom: 25, left: 10, right: 10),
                    child: (itemCount == 0)
                        ? loggedState.checkLoggedIn()
                            ? GestureDetector(
                                onTap: () {
                                  state.addFirst(item);
                                  state.calculateSum();
                                  setState(() {
                                    itemCount++;
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kPurpleColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Sepete Ekle',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text('Giriş Yapmalısınız.'),
                                        content: Text(
                                            "Ürün eklemek için profil kısmından giriş yapmalısınız."),
                                        actions: <Widget>[
                                          TextButton(
                                            child: Text(
                                              "Anladım.",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.all<
                                                      Color>(kGreyColor),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kPurpleColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Sepete Ekle',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  onPressed: !(itemCount == 0)
                                      ? (!(itemCount == 1)
                                          ? () {
                                              state.decrementItem(item);
                                              state.calculateSum();
                                              setState(() {
                                                itemCount--;
                                                print('this is itemcount' +
                                                    itemCount.toString());
                                              });
                                            }
                                          : () {
                                              state.deleteItem(item);
                                              setState(() {
                                                itemCount--;
                                                print('this is itemcount' +
                                                    itemCount.toString());
                                              });
                                              state.calculateSum();
                                            })
                                      : null,
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                  ),
                                  child: Text(
                                    '-',
                                    style: TextStyle(
                                        color: kPurpleColor,
                                        fontWeight: FontWeight.bold),
                                  )),
                              ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kPurpleColor),
                                ),
                                onPressed: null,
                                child: Text(
                                  itemCount.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                  ),
                                  onPressed: () {
                                    state.incrementItem(item);
                                    state.calculateSum();
                                    setState(() {
                                      itemCount++;
                                      print('this is itemcount' +
                                          itemCount.toString());
                                    });
                                  },
                                  child: Text(
                                    '+',
                                    style: TextStyle(
                                        color: kPurpleColor,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ],
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
