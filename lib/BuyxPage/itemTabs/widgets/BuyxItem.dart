import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/itemModal.dart';
import 'package:getir_app/BuyxPage/view/widgets/itemsDetailed.dart';
import 'package:getir_app/loggedInState/state/loggedInState.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class BuyxItem extends StatefulWidget {
  BuyxItem(
      {@required this.itemDescription,
      this.itemName,
      this.itemPrice,
      this.itemCount});
  final double itemPrice;
  final String itemName;
  final String itemDescription;
  final int itemCount;
  @override
  _BuyxItemState createState() => _BuyxItemState();
}

class _BuyxItemState extends State<BuyxItem> {
  bool isAdded = false;
  int numberAdded = 0;
  @override
  Widget build(BuildContext context) {
    var item = itemModal(
        priceItem: widget.itemPrice,
        descriptionItem: widget.itemDescription,
        nameItem: widget.itemName,
        numberOfItem: widget.itemCount);
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) =>
          Consumer<CartBuyxState>(builder: (context, state, widget) {
        if (state.checkExist(item)) {
          isAdded = true;
          numberAdded = state.getItemCount(item);
        } else {
          isAdded = false;
          numberAdded = 0;
        }
        return Container(
          padding: EdgeInsets.all(8),
          child: Stack(
            children: [
              GestureDetector(
                onTap: () async {
                  int count = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => itemsDetailed(
                                itemName: this.widget.itemName,
                                itemPrice: this.widget.itemPrice,
                                itemDescription: this.widget.itemDescription,
                                itemCount: numberAdded,
                              )));
                  print('this is count ' + count.toString());
                  setState(() {
                    numberAdded = count;
                    if (numberAdded > 0) {
                      isAdded = true;
                    }
                  });
                  print(numberAdded);
                  print(isAdded.toString());
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        border: Border.all(
                          width: 1,
                          color: kBrightPurpleColor,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/getirDoner.jpeg',
                        ),
                      ),
                    ),
                    Text(
                      this.widget.itemPrice.toString(),
                      style: TextStyle(
                          color: kPurpleColor, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      this.widget.itemName,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      this.widget.itemDescription,
                      style: TextStyle(
                          color: kGreyColor, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              isAdded
                  ? Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: MediaQuery.of(context).size.height / 9.5,
                        width: MediaQuery.of(context).size.width / 9.375,
                        child: Column(
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                onPressed: () {
                                  state.incrementItem(item);
                                  state.calculateSum();
                                  setState(() {
                                    numberAdded++;
                                  });
                                },
                                child: Text(
                                  '+',
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                              ),
                            ),
                            Expanded(
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kPurpleColor),
                                ),
                                onPressed: null,
                                child: Text(
                                  numberAdded.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Expanded(
                              child: ElevatedButton(
                                onPressed: () {
                                  state.decrementItem(item);
                                  state.calculateSum();
                                  setState(() {
                                    if (numberAdded == 1) {
                                      numberAdded--;
                                      isAdded = false;
                                      state.deleteItem(item);
                                    } else {
                                      numberAdded--;
                                    }
                                  });
                                },
                                child: Text(
                                  '-',
                                  style: TextStyle(
                                      color: kPurpleColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : loggedState.checkLoggedIn()
                      ? GestureDetector(
                          onTap: () {
                            state.addFirst(item);
                            state.calculateSum();
                            print(isAdded);
                            setState(() {
                              numberAdded++;
                              isAdded = true;
                            });
                            print(isAdded);
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: Card(
                                elevation: 10,
                                child: Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                ),
                              )),
                        )
                      : GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text('Giriş Yapmalısınız.'),
                                  content: Text(
                                      "Ürün eklemek için profil kısmından giriş yapmalısınız."),
                                  actions: <Widget>[
                                    TextButton(
                                      child: Text(
                                        "Anladım.",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                kGreyColor),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: Card(
                                elevation: 10,
                                child: Icon(
                                  Icons.add,
                                  color: kPurpleColor,
                                ),
                              )),
                        ),
            ],
          ),
        );
      }),
    );
  }
}
