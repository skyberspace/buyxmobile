import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';
import 'package:getir_app/BuyxPage/itemTabs/modals/tabModal.dart';
import 'package:getir_app/BuyxPage/itemTabs/widgets/BuyxItem.dart';
import 'package:getir_app/shoppingCartBuyx/state/cartState.dart';
import 'package:getir_app/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:indexed_list_view/indexed_list_view.dart';
import 'package:provider/provider.dart';

class BuyxTabs extends StatefulWidget {
  BuyxTabs({
    this.initPos,
    this.tabs,
  });
  final int initPos;
  final List<tabModal> tabs;
  @override
  _BuyxTabsState createState() => _BuyxTabsState();
}

class _BuyxTabsState extends State<BuyxTabs> {
  @override
  Widget build(BuildContext context) {
    int initPosition = widget.initPos;
    List<tabModal> tabs = widget.tabs;
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(),
                Text(
                  "Ürünler",
                  style: TextStyle(color: Colors.white),
                ),
                Consumer<CartBuyxState>(
                    builder: (context, state, widget) => state
                                .items.isNotEmpty ==
                            true
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CartPageBuyx()));
                            },
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(4),
                                          topLeft: Radius.circular(4)),
                                      color: Colors.white,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.shopping_basket,
                                        color: kPurpleColor,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(4),
                                          topRight: Radius.circular(4)),
                                      color: kBrightPurpleColor,
                                    ),
                                    child: Center(
                                      child: Text(
                                        state.sumOfAll.toString(),
                                        style: TextStyle(color: kPurpleColor),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : SizedBox()),
              ],
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          bottom: false,
          child: CustomTabView(
            initPosition: initPosition,
            itemCount: tabs.length,
            tabBuilder: (context, index) => Tab(text: tabs[index].nameItem),
            pageBuilder: (context, index) {
              return getirTabInside(tabs: tabs, position: index);
            },
            onPositionChange: (index) {
              print('current position: $index');
              initPosition = index;
            },
            onScroll: (position) => print('$position'),
          ),
        ),
      ),
    );
  }
}

/// Implementation

class CustomTabView extends StatefulWidget {
  final int itemCount;
  final IndexedWidgetBuilder tabBuilder;
  final IndexedWidgetBuilder pageBuilder;
  final Widget stub;
  final ValueChanged<int> onPositionChange;
  final ValueChanged<double> onScroll;
  final int initPosition;

  CustomTabView({
    @required this.itemCount,
    @required this.tabBuilder,
    @required this.pageBuilder,
    this.stub,
    this.onPositionChange,
    this.onScroll,
    this.initPosition,
  });

  @override
  _CustomTabsState createState() => _CustomTabsState();
}

class _CustomTabsState extends State<CustomTabView>
    with TickerProviderStateMixin {
  TabController controller;
  int _currentCount;
  int _currentPosition;

  @override
  void initState() {
    _currentPosition = widget.initPosition ?? 0;
    controller = TabController(
      length: widget.itemCount,
      vsync: this,
      initialIndex: _currentPosition,
    );
    controller.addListener(onPositionChange);
    controller.animation.addListener(onScroll);
    _currentCount = widget.itemCount;
    super.initState();
  }

  @override
  void didUpdateWidget(CustomTabView oldWidget) {
    if (_currentCount != widget.itemCount) {
      controller.animation.removeListener(onScroll);
      controller.removeListener(onPositionChange);
      controller.dispose();

      if (widget.initPosition != null) {
        _currentPosition = widget.initPosition;
      }

      if (_currentPosition > widget.itemCount - 1) {
        _currentPosition = widget.itemCount - 1;
        _currentPosition = _currentPosition < 0 ? 0 : _currentPosition;
        if (widget.onPositionChange is ValueChanged<int>) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (mounted) {
              widget.onPositionChange(_currentPosition);
            }
          });
        }
      }

      _currentCount = widget.itemCount;
      setState(() {
        controller = TabController(
          length: widget.itemCount,
          vsync: this,
          initialIndex: _currentPosition,
        );
        controller.addListener(onPositionChange);
        controller.animation.addListener(onScroll);
      });
    } else if (widget.initPosition != null) {
      controller.animateTo(widget.initPosition);
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.animation.removeListener(onScroll);
    controller.removeListener(onPositionChange);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.itemCount < 1) return widget.stub ?? Container();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          color: kSecondPurpleColor,
          alignment: Alignment.center,
          child: TabBar(
            isScrollable: true,
            controller: controller,
            labelColor: Colors.white,
            unselectedLabelColor: Colors.white,
            indicator: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: kYellowColor,
                  width: 2,
                ),
              ),
            ),
            tabs: List.generate(
              widget.itemCount,
              (index) => widget.tabBuilder(context, index),
            ),
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: controller,
            children: List.generate(
              widget.itemCount,
              (index) => widget.pageBuilder(context, index),
            ),
          ),
        ),
      ],
    );
  }

  onPositionChange() {
    if (!controller.indexIsChanging) {
      _currentPosition = controller.index;
      if (widget.onPositionChange is ValueChanged<int>) {
        widget.onPositionChange(_currentPosition);
      }
    }
  }

  onScroll() {
    if (widget.onScroll is ValueChanged<double>) {
      widget.onScroll(controller.animation.value);
    }
  }
}

class getirTabInside extends StatefulWidget {
  getirTabInside({@required this.tabs, this.position});
  final List<tabModal> tabs;
  final int position;

  @override
  _getirTabInsideState createState() => _getirTabInsideState();
}

class _getirTabInsideState extends State<getirTabInside> {
  var controller = IndexedScrollController(
    initialIndex: tabIndex,
  );
  static int tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          height: 30.0,
          width: 333.0,
          child: ListView.separated(
            itemCount: widget.tabs[widget.position].headerList.length,
            separatorBuilder: (BuildContext context, int index) => SizedBox(
              width: 10,
            ),
            primary: false,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    tabIndex = index;
                    controller.animateToIndex(index);
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: (tabIndex == index)
                      ? BoxDecoration(
                          color: kPurpleColor,
                          borderRadius: BorderRadius.circular(5))
                      : BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            width: 2,
                            color: kBrightPurpleColor,
                          )),
                  child: Center(
                    child: Text(
                        widget.tabs[widget.position].headerList[index].listName
                            .toString(),
                        style: (tabIndex == index)
                            ? TextStyle(color: Colors.white, fontSize: 14)
                            : TextStyle(color: kPurpleColor, fontSize: 14)),
                  ),
                ),
              );
            },
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Column(
        children: [
          Expanded(
            child: IndexedListView.separated(
              minItemCount: 0,
              maxItemCount: widget.tabs[widget.position].headerList.length - 1,
              separatorBuilder: (BuildContext context, int index) => SizedBox(
                height: 50,
              ),
              controller: controller,
              itemBuilder: (context, listViewIndex) {
                return (listViewIndex < 0 ||
                        listViewIndex >
                            widget.tabs[widget.position].headerList.length)
                    ? null
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            child: Text(
                              widget.tabs[widget.position]
                                  .headerList[listViewIndex].listName
                                  .toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: kGreyColor),
                            ),
                          ),
                          Material(
                            elevation: 10,
                            child: GridView.count(
                                primary: false,
                                shrinkWrap: true,
                                crossAxisCount: 3,
                                physics: NeverScrollableScrollPhysics(),
                                childAspectRatio: MediaQuery.of(context)
                                        .size
                                        .width /
                                    3 /
                                    (MediaQuery.of(context).size.height / 5),
                                children: List.generate(
                                  widget
                                      .tabs[widget.position]
                                      .headerList[listViewIndex]
                                      .itemList
                                      .length,
                                  (index) => BuyxItem(
                                    itemPrice: widget
                                        .tabs[widget.position]
                                        .headerList[listViewIndex]
                                        .itemList[index]
                                        .priceItem,
                                    itemDescription: widget
                                        .tabs[widget.position]
                                        .headerList[listViewIndex]
                                        .itemList[index]
                                        .descriptionItem,
                                    itemName: widget
                                        .tabs[widget.position]
                                        .headerList[listViewIndex]
                                        .itemList[index]
                                        .nameItem,
                                    itemCount: 1,
                                  ),
                                )),
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      );
              },
            ),
          ),
        ],
      ),
    );
  }
}
