import 'package:flutter/material.dart';

class itemModal {
  double priceItem;
  String nameItem;
  String descriptionItem;
  String moreDescriptionItem;
  int numberOfItem;
  itemModal(
      {@required this.priceItem,
      this.nameItem,
      this.descriptionItem,
      this.moreDescriptionItem,
      this.numberOfItem});
}
