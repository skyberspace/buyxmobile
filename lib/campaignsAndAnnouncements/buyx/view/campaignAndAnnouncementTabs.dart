import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/AnnouncementsBuyx.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/CampaignsBuyx.dart';
import 'package:getir_app/constants.dart';

class BuyxCampaignNAnnouncementsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "Buyx",
            style: TextStyle(
              color: kYellowColor,
              fontSize: 32,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Material(
                elevation: 2,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TabBar(
                    labelColor: kPurpleColor,
                    unselectedLabelColor: Colors.grey,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        color: kBrightPurpleColor,
                        borderRadius: BorderRadius.circular(10)),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Kampanyalar",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Duyurular",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: TabBarView(
                  // Tab Bar View
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    CampaignsBuyx(),
                    AnnouncementsBuyx(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
