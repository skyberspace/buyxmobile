import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/ReusableOpenedCard.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';

class CampaignRow extends StatelessWidget {
  const CampaignRow({
    @required this.campaign,
  });

  final campaignModal campaign;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      child: Card(
        child: Padding(
          padding:
              const EdgeInsets.only(top: 15, bottom: 15, left: 8, right: 8),
          child: Row(
            children: [
              Expanded(
                  flex: 3,
                  child: Image(image: AssetImage('assets/getirDoner.jpeg'))),
              Expanded(flex: 1, child: SizedBox()),
              Expanded(
                flex: 15,
                child: Text(
                  campaign.header,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                flex: 3,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => reusableOpenedCard(
                                  header: campaign.header,
                                  subHeader: campaign.subHeader,
                                  conditions: campaign.conditions,
                                )));
                  },
                  child: Card(
                    child: Icon(
                      Icons.more_vert,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
