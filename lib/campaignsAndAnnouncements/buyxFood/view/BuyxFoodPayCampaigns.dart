import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/buyx/widgets/campaignRow.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';

import '../../../constants.dart';

class BuyxFoodPayCampaigns extends StatelessWidget {
  List<campaignModal> campaigns = [
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
        ]),
    campaignModal(
        header: 'Meyve ve Sebze ürünlerinde 20 TL indirim!',
        subHeader:
            'Kampanyadan yararlanmak için sepetinize meyve sebze ürünlerinden eklemeniz gerekmektedir.',
        conditions: [
          '50 TL ve üzeri siparişinize meyve ve sebze ürünlerinde 20 tl indirim.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Size Özel 20 tl hediye!',
        subHeader:
            'Kampanyadan yararlanmak için sepetinize 120 TL üzerinde ürün eklemeniz gerekmektedir.',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Kampanyalar",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: kPurpleColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Card(
                          child: Icon(
                            Icons.add,
                            color: kPurpleColor,
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: SizedBox()),
                      Expanded(
                          flex: 15,
                          child: Text(
                            'Kampanya Kodu Ekle',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: kPurpleColor),
                          )),
                      Expanded(
                        flex: 3,
                        child: SizedBox(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: campaigns.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context, campaigns[index].header);
                    },
                    child: CampaignRow(
                      campaign: campaigns[index],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      backgroundColor: kBackGroundColor,
    );
  }
}
