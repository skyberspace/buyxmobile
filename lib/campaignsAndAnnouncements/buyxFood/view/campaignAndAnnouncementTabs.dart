import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/AnnouncementsBuyxFood.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/CampaignsBuyxFood.dart';
import 'package:getir_app/constants.dart';

class BuyxFoodCampaignNAnnouncementsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "buyx",
                  style: TextStyle(color: kYellowColor, fontSize: 30),
                ),
                TextSpan(
                    text: "yemek",
                    style: TextStyle(color: Colors.white, fontSize: 30)),
              ],
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Material(
                elevation: 2,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TabBar(
                    labelColor: kPurpleColor,
                    unselectedLabelColor: Colors.grey,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        color: kBrightPurpleColor,
                        borderRadius: BorderRadius.circular(10)),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Kampanyalar",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Duyurular",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: TabBarView(
                  // Tab Bar View
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    CampaignsGetirYemek(),
                    AnnouncementsGetirYemek(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
