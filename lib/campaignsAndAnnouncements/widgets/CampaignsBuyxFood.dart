import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';
import 'package:getir_app/constants.dart';
import 'ReusableCard.dart';
import 'ReusableOpenedCard.dart';

class CampaignsGetirYemek extends StatefulWidget {
  @override
  _CampaignsGetirYemekState createState() => _CampaignsGetirYemekState();
}

class _CampaignsGetirYemekState extends State<CampaignsGetirYemek> {
  List<campaignModal> campaigns = [
    campaignModal(
        header: 'GetirYemek size özel 20 tl indirim!',
        subHeader:
            'İndirimi kullanmak için minimum sepet tutarını geçmek zorundasınız.',
        conditions: [
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
        ]),
    campaignModal(
        header: 'Sıradaki 2 siparişinize özel toplamda 25 tl indirim.',
        subHeader: 'Restoran getirsine özel 2 siparişine toplam 25 tl indirim',
        conditions: [
          '50 TL ve üzeri siparişinize meyve ve sebze ürünlerinde 20 tl indirim.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Seçili Restoranlarda %50 indirim',
        subHeader: 'Seçili restoranlarda %50 indirim geçerlidir.',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Seçili Restoranlarda %50 indirim',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Seçili Restoranlarda %50 indirim',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: campaigns.length,
                  itemBuilder: (context, index) {
                    return OpenContainer(
                      closedColor: kBackGroundColor,
                      openElevation: 0.0,
                      closedElevation: 0.0,
                      transitionType: ContainerTransitionType.fade,
                      transitionDuration: const Duration(milliseconds: 1000),
                      useRootNavigator: true,
                      closedBuilder:
                          (BuildContext context, VoidCallback openContainer) {
                        // getting datas at index.
                        return ReusableCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                        );
                      },
                      openBuilder: (BuildContext context, VoidCallback _) {
                        return reusableOpenedCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                          conditions: campaigns[index].conditions,
                        );
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
