import 'package:flutter/material.dart';
import 'package:getir_app/constants.dart';

class ReusableCard extends StatelessWidget {
  const ReusableCard({@required this.header, this.subHeader});

  final String header;
  final String subHeader;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 2.5,
      padding: EdgeInsets.all(10),
      child: Card(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(5),
                child: Center(
                  child: FadeInImage.assetNetwork(
                      fit: BoxFit.cover,
                      placeholder: 'assets/placeholder.gif',
                      image:
                          "https://cdn.getir.com/misc/5ff82628eaffa13a73c6b0b7_banner_tr_1610905815267.jpeg"),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Text(
                  header,
                  style: TextStyle(
                    color: kPurpleColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Text(
                  subHeader,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
