import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';
import 'package:getir_app/constants.dart';
import 'ReusableCard.dart';
import 'ReusableOpenedCard.dart';

class CampaignsBuyx extends StatefulWidget {
  @override
  _CampaignsBuyxState createState() => _CampaignsBuyxState();
}

class _CampaignsBuyxState extends State<CampaignsBuyx> {
  List<campaignModal> campaigns = [
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
          'Su alınca şu şu olur',
        ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: campaigns.length,
                  itemBuilder: (context, index) {
                    return OpenContainer(
                      closedColor: kBackGroundColor,
                      openElevation: 0.0,
                      closedElevation: 0.0,
                      transitionType: ContainerTransitionType.fade,
                      transitionDuration: const Duration(milliseconds: 1000),
                      useRootNavigator: true,
                      closedBuilder:
                          (BuildContext context, VoidCallback openContainer) {
                        // getting datas at index.
                        return ReusableCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                        );
                      },
                      openBuilder: (BuildContext context, VoidCallback _) {
                        return reusableOpenedCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                          conditions: campaigns[index].conditions,
                        );
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
