import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/modals/campaignModal.dart';
import 'package:getir_app/constants.dart';
import 'ReusableCard.dart';
import 'ReusableOpenedCard.dart';
import 'package:animations/animations.dart';

class AnnouncementsBuyx extends StatefulWidget {
  @override
  _AnnouncementsBuyxState createState() => _AnnouncementsBuyxState();
}

class _AnnouncementsBuyxState extends State<AnnouncementsBuyx> {
  List<campaignModal> campaigns = [
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
        ]),
    campaignModal(
        header: 'Meyve ve Sebze ürünlerinde 20 TL indirim!',
        subHeader:
            'Kampanyadan yararlanmak için sepetinize meyve sebze ürünlerinden eklemeniz gerekmektedir.',
        conditions: [
          '50 TL ve üzeri siparişinize meyve ve sebze ürünlerinde 20 tl indirim.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Size Özel 20 tl hediye!',
        subHeader:
            'Kampanyadan yararlanmak için sepetinize 120 TL üzerinde ürün eklemeniz gerekmektedir.',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
    campaignModal(
        header: 'Getirde sularda %20 indirim!',
        subHeader:
            'Hemen alacağınız sularda 5L ve üzeri alışverişlerinizde %20 indirim!',
        conditions: [
          'Size Özel kampanyadır.',
          'Kampanya stoklarla sınırlıdır.',
          'Başka kampanya ile birleştirilemez',
          'Getir kampanya koşullarında değişiklik yapma hakkını saklı tutar.',
          'Örnek 1 Sepet 150 tl => 130 tl ödenir.',
          'GetirYemek ve getirBüyükte geçerli değildir.',
          'Bu kampanya bulunduğunuz yere özeldir.',
        ]),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: campaigns.length,
                  itemBuilder: (context, index) {
                    return OpenContainer(
                      closedColor: kBackGroundColor,
                      openElevation: 0.0,
                      closedElevation: 0.0,
                      transitionType: ContainerTransitionType.fade,
                      transitionDuration: const Duration(milliseconds: 1000),
                      useRootNavigator: true,
                      closedBuilder:
                          (BuildContext context, VoidCallback openContainer) {
                        // getting datas at index.
                        return ReusableCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                        );
                      },
                      openBuilder: (BuildContext context, VoidCallback _) {
                        return reusableOpenedCard(
                          header: campaigns[index].header,
                          subHeader: campaigns[index].subHeader,
                          conditions: campaigns[index].conditions,
                        );
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
