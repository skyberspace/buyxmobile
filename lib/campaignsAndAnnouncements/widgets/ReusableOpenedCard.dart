import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/widgets/ReusableCard.dart';
import 'package:getir_app/constants.dart';

class reusableOpenedCard extends StatelessWidget {
  const reusableOpenedCard(
      {@required this.header, this.subHeader, this.conditions});

  final String header;
  final String subHeader;
  final List<String> conditions;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: kPurpleColor,
        title: Text(
          "Kampanya Detay",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              ReusableCard(
                header: header,
                subHeader: subHeader,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Card(
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: conditions.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '- ' + conditions[index],
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
