import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyxFood.dart';
import 'package:getir_app/constants.dart';

class BuyxToBuyxFoodScreen extends StatefulWidget {
  @override
  _BuyxToBuyxFoodScreenState createState() => _BuyxToBuyxFoodScreenState();
}

class _BuyxToBuyxFoodScreenState extends State<BuyxToBuyxFoodScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 1), () async {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => NavigationBuyxFood()),
        (Route<dynamic> route) => false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPurpleColor,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          decoration: BoxDecoration(
            color: kYellowColor,
            shape: BoxShape.circle,
          ),
          child: Center(
              child: Text(
            "BuyxFood",
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: kPurpleColor,
            ),
          )),
        ),
      ),
    );
  }
}
