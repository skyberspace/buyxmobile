import 'package:flutter/material.dart';
import 'package:getir_app/Navigation/NavigationBuyx.dart';
import 'package:getir_app/constants.dart';

class splashScreen extends StatefulWidget {
  @override
  _splashScreenState createState() => _splashScreenState();
}

class _splashScreenState extends State<splashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3), () async {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => NavigationBuyx()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPurpleColor,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          decoration: BoxDecoration(
            color: kYellowColor,
            shape: BoxShape.circle,
          ),
          child: Center(
              child: Text(
            "Buyx",
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
              color: kPurpleColor,
            ),
          )),
        ),
      ),
    );
  }
}
