import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/buyxFood/view/campaignAndAnnouncementTabs.dart';
import 'package:getir_app/BuyxFoodPage/view/BuyxFoodPage.dart';
import 'package:getir_app/profilePage/view/profilePage.dart';
import 'package:getir_app/searchBuyxFood/view/SearchBuyxFoodPage.dart';
import 'package:getir_app/splash/BuyxFoodToBuyx.dart';
import 'package:getir_app/buyxActiveOrder/buyxActiveOrder.dart';

class NavigationBuyxFood extends StatefulWidget {
  @override
  _NavigationBuyxFoodState createState() => _NavigationBuyxFoodState();
}

class _NavigationBuyxFoodState extends State<NavigationBuyxFood> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    BuyxFoodPage(),
    SearchBuyxFoodPage(),
    BuyxActiveOrder(),
    ProfilePage(),
    BuyxFoodCampaignNAnnouncementsTab(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => BuyxFoodToBuyxScreen()));
        },
        tooltip: 'Increment',
        child: Icon(Icons.filter_tilt_shift),
        elevation: 2.0,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.cable_rounded,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_box,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.card_giftcard,
              size: 32,
            ),
            label: '',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xff5d3ebd),
        onTap: _onItemTapped,
      ),
    );
  }
}
