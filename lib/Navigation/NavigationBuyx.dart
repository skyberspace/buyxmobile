import 'package:flutter/material.dart';
import 'package:getir_app/campaignsAndAnnouncements/buyx/view/campaignAndAnnouncementTabs.dart';
import 'package:getir_app/BuyxPage/BuyxPage.dart';
import 'package:getir_app/profilePage/view/profilePage.dart';
import 'package:getir_app/searchBuyx/view/SearchBuyxPage.dart';
import 'package:getir_app/splash/BuyxToBuyxFood.dart';
import 'package:getir_app/buyxActiveOrder/buyxActiveOrder.dart';

class NavigationBuyx extends StatefulWidget {
  @override
  _NavigationBuyxState createState() => _NavigationBuyxState();
}

class _NavigationBuyxState extends State<NavigationBuyx> {
  void goToBuyxFood() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => NavigationBuyx()));
  }

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    BuyxPage(),
    SearchBuyxPage(),
    BuyxActiveOrder(),
    ProfilePage(),
    BuyxCampaignNAnnouncementsTab(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => BuyxToBuyxFoodScreen()));
        },
        tooltip: 'Increment',
        child: Icon(Icons.filter_tilt_shift),
        elevation: 2.0,
      ),*/
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.cable_rounded,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_box,
              size: 32,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.card_giftcard,
              size: 32,
            ),
            label: '',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xff5d3ebd),
        onTap: _onItemTapped,
      ),
    );
  }
}
